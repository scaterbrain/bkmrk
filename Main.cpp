//#include <gtkmm/application.h>
#include "include/MainWindow.h"


int main(int argc, char* argv[])
{
    Glib::RefPtr<Gtk::Application> app = Gtk::Application::create(argc, argv, "com.sctrbrn.bkmrk");

    MainWindow window;

    return app->run(window);
}
