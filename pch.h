#ifndef PCH_H_INCLUDED
#define PCH_H_INCLUDED

//#include <bits/stdc++.h>
#include <exception>
#include <functional>
#include <iostream>
#include <optional>
#include <regex>
#include <regex>
#include <string>
#include <stdexcept>
#include <tuple>
#include <vector>
//#include <future>
//#include <thread>
//#include <chrono>

#include <sqlite3.h>


//#include <gtkmm-3.0/gtkmm/application.h>
#include <gtkmm/application.h>
#include <gtkmm/actionbar.h>
#include <gtkmm/box.h>
#include <gtkmm/button.h>
#include <gtkmm/buttonbox.h>
#include <gtkmm/checkbutton.h>
#include <gtkmm/comboboxtext.h>
#include <gtkmm/dialog.h>
#include <gtkmm/entry.h>
#include <gtkmm/grid.h>
#include <gtkmm/headerbar.h>
#include <gtkmm/infobar.h>
#include <gtkmm/image.h>
#include <gtkmm/iconview.h>
#include <gtkmm/label.h>
#include <gtkmm/liststore.h>
//#include <gtkmm/menutoolbutton.h>
#include <memory>
#include <gtkmm/messagedialog.h>
#include <gtkmm/modelbutton.h>
#include <gtkmm/notebook.h>
#include <gtkmm/popover.h>
//#include <gtkmm/popovermenu.h>
#include <gtkmm/scale.h>
#include <gtkmm/separator.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/stackswitcher.h>
#include <gtkmm/searchentry.h>
#include <gtkmm/separatormenuitem.h>
#include <gtkmm/spinbutton.h>
#include <gtkmm/textview.h>
#include <gtkmm/togglebutton.h>
#include <gtkmm/toolbutton.h>
#include <gtkmm/treeview.h>
#include <gtkmm/treemodelcolumn.h>


#endif // PCH_H_INCLUDED
