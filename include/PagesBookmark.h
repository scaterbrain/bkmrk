#ifndef PAGESBOOKMARK_H
#define PAGESBOOKMARK_H

#include <string>
#include <gtkmm/button.h>
//#include <gtkmm/buttonbox.h>
//#include <gtkmm/comboboxtext.h>
#include <gtkmm/grid.h>
#include <gtkmm/liststore.h>
//#include <gtkmm/menu.h>
//#include <gtkmm/menuitem.h>
#include <gtkmm/modelbutton.h>
#include <gtkmm/popover.h>
#include <gtkmm/popovermenu.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/searchentry.h>
#include <gtkmm/treemodelcolumn.h>
#include <gtkmm/treeview.h>
#include <gtkmm/togglebutton.h>

#include "Bookmark.h"
#include "BookmarkRepository.h"
#include "Paginate.h"
#include "SearchTagPopover.h"


class PagesBookmark : public Gtk::Grid
{
    public:
        PagesBookmark();
        virtual ~PagesBookmark();

    protected:
        class ModelColumns : public Gtk::TreeModelColumnRecord
        {
            public:
                Gtk::TreeModelColumn<int> m_col_id;
                Gtk::TreeModelColumn<std::string> m_col_title;
                Gtk::TreeModelColumn<int> m_col_rating;
                Gtk::TreeModelColumn<int> m_col_link_id;
                Gtk::TreeModelColumn<std::string> m_col_link_uri;

                ModelColumns()
                {
                    add(m_col_id);
                    add(m_col_title);
                    add(m_col_rating);
                    add(m_col_link_id);
                    add(m_col_link_uri);
                }
        };

    private:
        BookmarkRepository repository;
        Gtk::Box m_SearchBox;
        Gtk::Button m_TagCb;
        Gtk::SearchEntry m_SearchEntry;
        Gtk::Box m_BtnBox;
        Gtk::ToggleButton m_BtnAdd;
        Gtk::Button m_BtnDelete;
        Gtk::ScrolledWindow m_ScrlledWindow;
        ModelColumns m_Columns;
        Gtk::TreeView m_TvBookmark;
        Glib::RefPtr<Gtk::ListStore> m_RefTreeModel;
        Paginate m_Paginate;
        SearchTagPopover m_TagPopover;
        Gtk::Popover m_CreatePopover;
        //Gtk::PopoverMenu m_CreatePopoverMenu;
        Gtk::ModelButton m_CreateBookmark;
        Gtk::ModelButton m_CreateTag;
        Gtk::Box m_CreatePopoverBox;
        unsigned int m_TagId = 0;

        void FillTvBookmarks(unsigned int pages);
        void OpenBookmarkForm(const unsigned int& ids);
        void ConfigureCreatePopover();

        // Signal Handlers
        void OnSearching();
        void OnClickAdd();
        void OnClickDelete();
        void OnSelectTv(const Gtk::TreeModel::Path& path, Gtk::TreeViewColumn* column);
        void OnClickNext();
        void OnClickPrev();
        void OnHideCreatePopover();
        void OnClickItemBookmark();
        void OnClickItemTag();

        // Callback
        void OnTagSelected(const bool& a, const int& b, const std::string& message);
};

#endif // PAGESBOOKMARK_H
