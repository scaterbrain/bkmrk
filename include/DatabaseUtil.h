#ifndef DATABASEUTIL_H
#define DATABASEUTIL_H

#include <sqlite3.h>


class DatabaseUtil
{
    public:
        // Delete copy constructor
        DatabaseUtil(const DatabaseUtil& other) = delete;
        static DatabaseUtil& GetInstance()
        {
            return m_instance;
        };
    public:
        void Bind(const unsigned int& index, const unsigned int& p);
        void Bind(const unsigned int& index, const char* p);
        void BeginTrasaction();
        int CloseConnection();
        void CommitTransaction();
        int ExecutesStatement();
        void FinalizeStatement();
        const char* GetRowStr(const unsigned int& index);
        unsigned int GetRowInt(const unsigned int& index);
        long unsigned int GetRowLong(const unsigned int& index);
        double GetRowDouble(const unsigned int& index);
        unsigned int GetLastRowInsertId() const;
        const char* GetErrors();
        int OpenConnection();
        bool PreparedStatement(const char* sql);
        void RollbackTransaction();
    private:
        virtual ~DatabaseUtil();
        DatabaseUtil();
    private:
        void ClearStatement();
        void LogSql(const int& code_err, const char* message);
        static DatabaseUtil m_instance;
        sqlite3*            m_connection = nullptr;
        sqlite3_stmt*       m_stmt = nullptr;
};

#endif // DATABASEUTIL_H
