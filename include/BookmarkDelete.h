#ifndef BOOKMARKDELETE_H
#define BOOKMARKDELETE_H

#include <gtkmm/dialog.h>
#include <gtkmm/box.h>
#include <gtkmm/label.h>
#include <gtkmm/separator.h>
#include <gtkmm/button.h>


class BookmarkDelete : public Gtk::Dialog
{
    public:
        BookmarkDelete(const unsigned int& bookmarkId);
        virtual ~BookmarkDelete();

    private:
        const unsigned int m_bookmark_id;
        Gtk::Box m_content_box;
        Gtk::Label m_content_label;
        Gtk::Separator m_separator;
        Gtk::Box m_btn_box;
        Gtk::Button m_btn_cancel;
        Gtk::Button m_btn_submit;
        void onSubmit();
        void onCancel();
};

#endif // BOOKMARKDELETE_H
