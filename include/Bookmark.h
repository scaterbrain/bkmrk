#ifndef BOOKMARK_H
#define BOOKMARK_H

#include "Link.h"
#include "Tag.h"
#include <string>
#include <vector>

class Bookmark
{
    public:
        Bookmark(const std::string& title, const Link& link);
        Bookmark(const unsigned int& ids, const std::string& title, const Link& link);
        virtual ~Bookmark();

        unsigned int getId() const;
        std::string getTitle() const;
        std::string getDescription() const;
        bool getHidden() const;
        double getRating() const;
        Link getLink() const;
        std::vector<scaterbrain::Tag> getTags() const;

        void setId(const unsigned int& val);
        void setTitle(const std::string& val);
        void setLink(const Link& link);
        void setRating(const double& rating);
        void setHidden(const bool& value);
        void setDescription(const std::string& description);
        void addTags(const scaterbrain::Tag& t);
        void setTags(const std::vector<scaterbrain::Tag>& tags);

    private:
        unsigned int m_ids;
        std::string m_title;
        bool m_hidden;
        double m_rating;
        std::string m_description;
        Link m_link;
        std::vector<scaterbrain::Tag> m_tags;
};

#endif // BOOKMARK_H
