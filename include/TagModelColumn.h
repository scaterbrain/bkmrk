#ifndef TAGMODELCOLUMN_H
#define TAGMODELCOLUMN_H

#include <gtkmm/treemodel.h>
#include <gtkmm/treemodelcolumn.h>
#include <gdkmm/pixbuf.h>


class TagModelColumn : public Gtk::TreeModel::ColumnRecord
{
    public:
        TagModelColumn();
        virtual ~TagModelColumn();

        Gtk::TreeModelColumn<unsigned int> col_id;
        Gtk::TreeModelColumn<Glib::ustring> col_name;
        Gtk::TreeModelColumn<double> col_rating;
        Gtk::TreeModelColumn<Glib::RefPtr<Gdk::Pixbuf>> col_pixbuf;
};

#endif // TAGMODELCOLUMN_H
