#ifndef FORMBOOKMARK_H
#define FORMBOOKMARK_H

#include <gtkmm/dialog.h>
#include <gtkmm/label.h>
#include <gtkmm/entry.h>
#include <gtkmm/grid.h>
#include <gtkmm/button.h>
#include <gtkmm/scale.h>
#include <gtkmm/checkbutton.h>
#include <gtkmm/box.h>
#include <gtkmm/separator.h>
#include <gtkmm/textview.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/infobar.h>
#include <gtkmm/togglebutton.h>
#include <gtkmm/liststore.h>
#include <gtkmm/treeview.h>
#include <gtkmm/notebook.h>

#include "Bookmark.h"
#include "Tag.h"
#include "BookmarkRepository.h"
#include "TagModelColumn.h"
#include "SearchTagPopover.h"
#include "InfoBar.h"


class FormBookmark : public Gtk::Dialog
{
    public:
        FormBookmark(const unsigned int& bookmark);
        virtual ~FormBookmark();

    private:
        const unsigned int m_bookmark_id;
        const bool isEditMode() const;
        BookmarkRepository repository;

        scaterbrain::InfoBar m_Infobar;
        std::vector<scaterbrain::Tag> m_tags;

        Gtk::Grid m_grid;
        Gtk::Label m_url_label;
        Gtk::Entry m_url_entry;
        Gtk::Label m_title_label;
        Gtk::Entry m_title_entry;
        Gtk::Label m_hidden_lbl;
        Gtk::CheckButton m_hidden_cb;
        Gtk::Label m_rating_label;
        Gtk::Scale m_rating_entry;
        Gtk::ScrolledWindow m_desc_scrl;
        Gtk::TextView m_desc_entry;
        Glib::RefPtr<Gtk::TextBuffer> m_desc_buffer;
        Gtk::Separator m_separator2;
        Gtk::Notebook m_tab;
        Gtk::Box m_box_current_tags;
        Gtk::ScrolledWindow m_scrl_tv_current_tags;
        Gtk::TreeView m_tv_current_tags;
        Gtk::Box m_tool_current_tags;
        Gtk::Button m_btn_delete_current_tags;

        Gtk::Separator m_separator;
        Gtk::Box m_footer_box;
        Gtk::Button m_btn_cancel;
        Gtk::Button m_btn_submit;

        // Tab
        TagModelColumn m_TagModelColumn;
        Glib::RefPtr<Gtk::ListStore> m_refTreeModel;
        SearchTagPopover m_SearchTagPopover;

        void fillTags();
        void addTagsPreview(const scaterbrain::Tag& tag);
        bool validatesForm(std::string& message);
        bool isNewTags(const unsigned int& pid);
        bool isDeleteTags(const unsigned int& pid);
        bool isAlreadyExist(const unsigned int& pid);
        void configureTabs();

        // Signals handler
        void onCancelForm();
        void onSubmitForm();
        void onShowForm();
        void onClickRemoveTags();
        void onTvCurrentTagsRowRemoved(const Gtk::TreeModel::Path& path);

        // Callback
        void OnTagPopoverSelected(const bool& a, const int& b, const std::string& message);
};

#endif // FORMBOOKMARK_H
