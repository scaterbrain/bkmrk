#ifndef DELETECONFIRMATIONDIALOG_H
#define DELETECONFIRMATIONDIALOG_H

#include <gtkmm/actionbar.h>
#include <gtkmm/button.h>
#include <gtkmm/box.h>
#include <gtkmm/dialog.h>
#include <gtkmm/grid.h>
#include <gtkmm/image.h>
#include <gtkmm/label.h>


class DeleteConfirmationDialog : public Gtk::Dialog
{
    public:
        DeleteConfirmationDialog(const char* extra_message);
        virtual ~DeleteConfirmationDialog();

    private:
        const char* m_ExtraMessage;
        Gtk::Label m_MessageLbl;
        Gtk::Label m_ExtraMessageLbl;
        Gtk::Image m_Image;
        Gtk::Box m_BoxMain;
        Gtk::Grid m_Grid;
        Gtk::ActionBar m_ActionBar;
        Gtk::Button m_SubmitBtn;
        Gtk::Button m_CancelBtn;
        void OnSubmitClicked();
        void OnCancelClicked();
        void ConfigureMain();
        void ConfigureFooter();
};

#endif // DELETECONFIRMATIONDIALOG_H
