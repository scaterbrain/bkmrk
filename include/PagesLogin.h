#ifndef PAGESLOGIN_H
#define PAGESLOGIN_H

#include <functional>
#include <string>
#include <gtkmm/button.h>
#include <gtkmm/entry.h>
#include <gtkmm/grid.h>
#include <gtkmm/label.h>

#include "InfoBar.h"


class PagesLogin : public Gtk::Grid
{
    public:
        PagesLogin(std::function<void(bool, std::string)> callback, scaterbrain::InfoBar& infoBar);
        virtual ~PagesLogin();

    private:
        Gtk::Label m_username_lbl;
        Gtk::Label m_password_lbl;
        Gtk::Entry m_username_entry;
        Gtk::Entry m_password_entry;
        Gtk::Button m_btn_login;
        void onLoginClick();
        std::function<void(bool, std::string)> m_callback;
        scaterbrain::InfoBar& r_InfoBar;
};

#endif // PAGESLOGIN_H
