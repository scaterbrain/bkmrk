#ifndef INFOBAR_H
#define INFOBAR_H

#include <gtkmm/infobar.h>
#include <gtkmm/label.h>
#include <gtkmm/image.h>


namespace scaterbrain {

    class InfoBar : public Gtk::InfoBar
    {
        public:
            InfoBar();
            virtual ~InfoBar();
        public:
           void SetErrors(const char* message, const Gtk::MessageType& type);

        private:
            Gtk::Label m_MessageLabel;
            Gtk::Image m_IconImage;
            void OnInfoBarResponse(const int& reponse);
    };
}

#endif // INFOBAR_H
