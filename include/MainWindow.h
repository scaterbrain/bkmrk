#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <string>
#include <memory>

#include <gtkmm/box.h>
#include <gtkmm/grid.h>
#include <gtkmm/modelbutton.h>
#include <gtkmm/stack.h>
#include <gtkmm/stackswitcher.h>
#include <gtkmm/window.h>

#include "PagesLogin.h"
#include "PagesRegister.h"
#include "PagesBookmark.h"
#include "HeaderBar.h"
#include "InfoBar.h"


class MainWindow : public Gtk::Window
{
    public:
        MainWindow();
        virtual ~MainWindow();
    private:
        Gtk::Grid m_Grid;
        scaterbrain::InfoBar m_InfoBar;
        Gtk::HBox m_StackSwitcherWrapper;
        Gtk::StackSwitcher m_StackSwitcher;
        Gtk::Stack m_Stack;
        PagesLogin m_PagesLogin;
        PagesRegister m_PagesRegister;
        Gtk::ModelButton m_LogoutBtn;
        HeaderBar m_HeaderBar;
        std::unique_ptr<PagesBookmark> m_PagesBookmarkPtr;

        // Callback
        void LoginCallback(bool flag, std::string messages);
        void LogoutCallback();

        // Signal Handlers
        void OnStackChange();
};

#endif // MAINWINDOW_H
