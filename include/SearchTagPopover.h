#ifndef SEARCHTAGPOPOVER_H
#define SEARCHTAGPOPOVER_H

#include <gtkmm/button.h>
#include <gtkmm/grid.h>
#include <gtkmm/iconview.h>
#include <gtkmm/liststore.h>
#include <gtkmm/togglebutton.h>
#include <gtkmm/popover.h>
#include <gtkmm/searchentry.h>
#include <gtkmm/scrolledwindow.h>

#include "TagModelColumn.h"
#include "Tag.h"


class SearchTagPopover : public Gtk::ToggleButton
{
    public:
        SearchTagPopover();
        virtual ~SearchTagPopover();

        void ShowDescription(const bool& visible);
        void ShowUnsetButton(const bool& visible);
        void SetIcon(const std::string& icon_name);

        // Signal Custom
        typedef sigc::signal<void, bool, int, std::string> type_signal_on_row_activated;
        type_signal_on_row_activated SignalOnRowActivated();

    private:
        Gtk::Grid m_Grid;
        Gtk::Popover m_Popover;
        Gtk::SearchEntry m_SearchEntry;
        Gtk::ScrolledWindow m_ScrolledWindow;
        Gtk::IconView m_IconView;
        Glib::RefPtr<Gtk::ListStore> m_refListModel;
        TagModelColumn m_TagModelColumn;
        Gtk::Button m_UnsetBtn;
        void AddRow(const scaterbrain::Tag& tag);
        bool m_ShowDescription;
        Gtk::Menu m_MenuEditPopup;

        // Signal Custom
        type_signal_on_row_activated m_SignalOnRowActivated;

        // Signal
        void OnClick();
        void OnRowActivated(const Gtk::TreeModel::Path& path);
        void FillTreeView();
        void OnSearch();
        void OnClickUnset();
        void OnHidePopover();
        bool OnRightClickIconView(GdkEventButton* button_event);
        void OnClickEdit();
};

#endif // SEARCHTAGPOPOVER_H
