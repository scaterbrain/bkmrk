#ifndef TAG_H
#define TAG_H

#include <string>


namespace scaterbrain
{
    class Tag
    {
        public:
            Tag(const unsigned int& pid);
            Tag(const std::string& pname);
            Tag(const std::string& pname, const unsigned int& rating);
            Tag(const unsigned int& pid, const std::string& pname);
            Tag(const unsigned int& pid, const std::string& pname, const double& rating);
            virtual ~Tag();
            unsigned int GetId() const;
            std::string GetName() const;
            double GetRating() const;

        private:
            unsigned int m_Id;
            std::string m_Name;
            double m_Rating;
    };
}

#endif // TAG_H
