#ifndef PAGESREGISTER_H
#define PAGESREGISTER_H

#include "InfoBar.h"

#include <gtkmm/button.h>
#include <gtkmm/grid.h>
#include <gtkmm/label.h>
#include <gtkmm/entry.h>


class PagesRegister : public Gtk::Grid
{
    public:
        PagesRegister(scaterbrain::InfoBar& infoBar);
        virtual ~PagesRegister();

    private:
        scaterbrain::InfoBar& r_InfoBar;
        Gtk::Label m_email_lbl;
        Gtk::Entry m_email_entry;
        Gtk::Label m_pswd_lbl;
        Gtk::Entry m_pswd_entry;
        Gtk::Label m_cpswd_lbl;
        Gtk::Entry m_cpswd_entry;
        Gtk::Button m_regis_btn;
        void OnClickRegister();
};

#endif // PAGESREGISTER_H
