#ifndef SESSION_H
#define SESSION_H


class Session
{
    public:
        virtual ~Session();
        static const unsigned int& GetCurrentUser();
        static void SetCurrentUser(const unsigned int& users_id);
        static void RemoveCurrentUser();
    private:
        Session() = delete;
        static unsigned int m_CurrentUser;
};

#endif // SESSION_H
