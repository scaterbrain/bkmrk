#ifndef HEADERBAR_H
#define HEADERBAR_H

#include <gtkmm/box.h>
#include <gtkmm/comboboxtext.h>
#include <gtkmm/headerbar.h>
#include <gtkmm/modelbutton.h>
#include <gtkmm/popover.h>
#include <gtkmm/togglebutton.h>
#include <gtkmm/separatormenuitem.h>


class HeaderBar : public Gtk::HeaderBar
{
    public:
        HeaderBar(Gtk::ModelButton& logout_btn);
        virtual ~HeaderBar();
        void ShowUserToolButton();

    private:
        Gtk::ToggleButton m_UserMenuToggle;
        Gtk::Popover m_UserMenuPopover;
        Gtk::ModelButton m_BtnAbout;
        Gtk::ModelButton m_BtnProfile;
        Gtk::SeparatorMenuItem m_Separator;
        Gtk::ModelButton& r_BtnLogout;
        Gtk::Box m_Box;
        Gtk::ComboBoxText m_ComboBox;

        void ConfigureComboBox();
        void ConfigureTestMenu();
        void ConfigureUserMenu();

        // Signal
        void OnClickUserMenuToggle();
        void OnHideUserMenu();
        void OnClickLogoutBtn();
};

#endif // HEADERBAR_H
