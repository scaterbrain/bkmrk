#ifndef COMMONUTIL_H
#define COMMONUTIL_H

#include <string>


class CommonUtil
{
    public:
        virtual ~CommonUtil();
        static std::string trim(std::string&& yourString);
        static std::string appendFirstAndLast(const std::string& yourString, const std::string& appendString);
        static bool isValidUrl(const std::string&& urls);
        static bool isLengthValid(const std::string& yourString, const unsigned int& min, const unsigned int& max);
        static bool isValidEmail(const std::string& email);
    private:
        CommonUtil();
};

#endif // COMMONUTIL_H
