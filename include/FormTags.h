#ifndef FORMTAGS_H
#define FORMTAGS_H

#include <gtkmm/dialog.h>
#include <gtkmm/label.h>
#include <gtkmm/entry.h>
#include <gtkmm/grid.h>
#include <gtkmm/button.h>
#include <gtkmm/actionbar.h>
#include <gtkmm/infobar.h>
#include <gtkmm/spinbutton.h>

#include "BookmarkRepository.h"
#include "InfoBar.h"
#include "Tag.h"


class FormTags : public Gtk::Dialog
{
    public:
        FormTags();
        FormTags(const scaterbrain::Tag& tag);
        virtual ~FormTags();

    private:
        const unsigned int m_TagId;
        BookmarkRepository m_Repository;
        Gtk::Grid m_Grid;
        Gtk::Label m_NameLabel;
        Gtk::Entry m_NameEntry;
        Gtk::Label m_RatingLabel;
        Gtk::SpinButton m_RatingEntry;
        Gtk::Button m_BtnSubmit;
        Gtk::Button m_BtnCancel;
        Gtk::Button m_BtnDelete;
        Gtk::ActionBar m_ActionBar;
        scaterbrain::InfoBar m_InfoBar;
    private:
        void Init();
        void OnClickSubmit();
        void OnClickCancel();
        void OnClickDelete();
};

#endif // FORMTAGS_H
