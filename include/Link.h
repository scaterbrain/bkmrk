#ifndef LINK_H
#define LINK_H

#include <string>


class Link
{
    public:
        Link(const unsigned int& id);
        Link(const std::string& url);
        Link(const unsigned int& ids, const std::string& url);
        virtual ~Link();

        unsigned int getIds() const;
        std::string getUrls() const;
        void setIds(const unsigned int& val);
        void setUrls(const std::string& val);

    private:
        unsigned int m_ids;
        std::string m_urls;
};

#endif // LINK_H
