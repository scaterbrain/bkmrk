#ifndef LOGINWINDOW_H
#define LOGINWINDOW_H

#include <gtkmm/window.h>
#include <gtkmm/entry.h>
#include <gtkmm/grid.h>
#include <gtkmm/button.h>
#include <gtkmm/label.h>

class LoginWindow : public Gtk::Window
{
    public:
        LoginWindow();
        virtual ~LoginWindow();

    protected:

    private:
        Gtk::Grid m_grid;
        Gtk::Label m_username_lbl;
        Gtk::Label m_password_lbl;
        Gtk::Entry m_username_entry;
        Gtk::Entry m_password_entry;
        Gtk::Button m_btn_login;
        void onLoginClick();
};

#endif // LOGINWINDOW_H
