#ifndef PAGINATE_H
#define PAGINATE_H

#include <gtkmm/box.h>
#include <gtkmm/button.h>
#include <gtkmm/label.h>


class Paginate : public Gtk::Box
{
    public:
        Paginate();
        virtual ~Paginate();
    public:
        Gtk::Label m_InfoLbl;
        Gtk::Button m_NextBtn;
        Gtk::Button m_PrevBtn;
        void SetTotalData(const unsigned int& total_data);
        unsigned int Next();
        unsigned int Prev();
        unsigned int GetCurrentPages() const;
        unsigned int GetTotalPages() const;
        unsigned int GetRowPerPages() const;
    private:
        Gtk::Label m_TotalLbl;
        unsigned int m_CurrentPage = 0;
        unsigned int m_TotalData = 0;
        unsigned int m_TotalPages = 0;
        unsigned int m_RowPerPages = 10;
        void RefreshUi();
};

#endif // PAGINATE_H
