#ifndef BOOKMARKREPOSITORY_H
#define BOOKMARKREPOSITORY_H

#include <vector>
#include <optional>

#include "Bookmark.h"
#include "Tag.h"


class BookmarkRepository final
{
    public:
        BookmarkRepository();
        virtual ~BookmarkRepository();
    public:
        void Create(const Bookmark& b, std::string& outputMessages);
        void Create(const scaterbrain::Tag& t, std::string& outputMessages);
        void Delete(const unsigned int& bookmarkId, std::string& outputMessages);
        void Delete(const scaterbrain::Tag& tag, std::string& outputMessages);
        unsigned int CountTags(const unsigned int& bookmarkId);
        std::optional<Bookmark> FindById(const unsigned int& ids);
        std::vector<Bookmark> FindAll(const std::string& search, const unsigned int& pages, const unsigned int& ptags, std::string& out_messages);
        std::vector<scaterbrain::Tag> FindAll(const std::string& psearch, const unsigned short& limit);
        bool Login(const std::string& username, const std::string& password, unsigned int& out_users_id, std::string& out_messages);
        void Register(const std::string& username, const std::string& psw, std::string& outMessage);
        void Update(const Bookmark& b, const std::vector<scaterbrain::Tag>& delete_tags, std::string& outputMessages);
        void Update(const scaterbrain::Tag& tag, std::string& outputMessages);
    private:
        unsigned int CountAllBookmark(const char* psearch);
        bool DeleteBookmark(const unsigned int& bookmarkId, std::string& outputMessages);
        bool DeleteBookmarkTags(const unsigned int& bookmarkId, std::string& outputMessages);
        bool DeleteBookmarkTags(const unsigned int& bookmarkId, const unsigned int& tagsId, std::string& outputMessages);
        int FindOrCreateLink(Link&& link, std::string& outputMessages);
        std::vector<scaterbrain::Tag> GetTags(const unsigned int& bookmarkId);
        int InsertBookmark(const Bookmark& bookmark, const unsigned int& linkId, std::string& outputMessages);
        void InsertBookmarkTags(const unsigned int& tagId, const unsigned int& bookmarkId, std::string& outputMessages);
        void UpdateBookmark(const Bookmark& bookmark, std::string& outputMessages);
};

#endif // BOOKMARKREPOSITORY_H
