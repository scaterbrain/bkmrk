#include "Tag.h"


namespace scaterbrain
{

    Tag::Tag(const unsigned int& pid) :
        m_Id(pid)
    {}

    Tag::Tag(const std::string& pname) :
        m_Name(pname)
    {}

    Tag::Tag(const std::string& pname, const unsigned int& rating):
        m_Name(pname), m_Rating(rating)
    {}

    Tag::Tag(const unsigned int& pid, const std::string& pname):
        m_Id(pid), m_Name(pname)
    {}

    Tag::Tag(const unsigned int& pid, const std::string& pname, const double& rating):
        m_Id(pid), m_Name(pname), m_Rating(rating)
    {}

    Tag::~Tag()
    {}

    unsigned int Tag::GetId() const
    {
        return m_Id;
    }

    std::string Tag::GetName() const
    {
        return m_Name;
    }

    double Tag::GetRating() const
    {
        return m_Rating;
    }
};
