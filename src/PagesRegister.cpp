#include "PagesRegister.h"
#include "BookmarkRepository.h"
#include "CommonUtil.h"


PagesRegister::PagesRegister(scaterbrain::InfoBar& infoBar) :
    r_InfoBar(infoBar),
    m_email_lbl("Email: "),
    m_pswd_lbl("Password: "),
    m_cpswd_lbl("Confirm: "),
    m_regis_btn("Register")
{
    m_pswd_entry.set_visibility(false);
    m_cpswd_entry.set_visibility(false);

    attach(m_email_lbl, 0, 0, 1, 1);
    attach_next_to(m_email_entry, m_email_lbl, Gtk::PositionType::POS_RIGHT, 1, 1);
    attach_next_to(m_pswd_entry, m_email_entry, Gtk::PositionType::POS_BOTTOM, 1, 1);
    attach_next_to(m_cpswd_entry, m_pswd_entry, Gtk::PositionType::POS_BOTTOM, 1, 1);
    attach_next_to(m_pswd_lbl, m_email_lbl, Gtk::PositionType::POS_BOTTOM, 1, 1);
    attach_next_to(m_cpswd_lbl, m_pswd_lbl, Gtk::PositionType::POS_BOTTOM, 1, 1);
    attach_next_to(m_regis_btn, m_cpswd_entry, Gtk::PositionType::POS_BOTTOM, 1, 1);

    set_halign(Gtk::ALIGN_CENTER);
    set_margin_top(10);
    set_column_spacing(10);
    set_row_spacing(7);

    m_regis_btn.signal_clicked().connect(sigc::mem_fun(*this, &PagesRegister::OnClickRegister));
}

PagesRegister::~PagesRegister()
{}

void PagesRegister::OnClickRegister()
{
    m_email_entry.set_text(CommonUtil::trim(m_email_entry.get_text()));

    if (!CommonUtil::isValidEmail(m_email_entry.get_text())) {
        r_InfoBar.SetErrors("Email is not valid", Gtk::MessageType::MESSAGE_WARNING);
        return;
    }

    if (!CommonUtil::isLengthValid(m_pswd_entry.get_text(), 3, 20)) {
        r_InfoBar.SetErrors("Password length is not valid", Gtk::MessageType::MESSAGE_WARNING);
        return;
    }

    if (!CommonUtil::isLengthValid(m_cpswd_entry.get_text(), 3, 20)) {
        r_InfoBar.SetErrors("Password confirmation length is not valid", Gtk::MessageType::MESSAGE_WARNING);
        return;
    }

    if (m_pswd_entry.get_text() != m_cpswd_entry.get_text()) {
        r_InfoBar.SetErrors("Password missmatch", Gtk::MessageType::MESSAGE_WARNING);
        return;
    }

    std::string output_message;
    BookmarkRepository repo;

    repo.Register(m_email_entry.get_text(), m_pswd_entry.get_text(), output_message);

    if (output_message.empty()) {
        r_InfoBar.SetErrors("User has been created", Gtk::MessageType::MESSAGE_INFO);
        return;
    }

    r_InfoBar.SetErrors(output_message.c_str(), Gtk::MessageType::MESSAGE_ERROR);
}
