#include "DatabaseUtil.h"

#include <sqlite3.h>
#include <iostream>
#include <sstream>


DatabaseUtil::DatabaseUtil()
{
    //ctor
}


DatabaseUtil::~DatabaseUtil()
{
    //dtor
}

DatabaseUtil DatabaseUtil::m_instance;


void DatabaseUtil::BeginTrasaction()
{
    //sqlite3_busy_timeout(m_connection, 3000);
    int result = sqlite3_exec(m_connection, "BEGIN TRANSACTION; ", NULL, NULL, NULL);
    //int result = sqlite3_exec(m_connection, "BEGIN IMMEDIATE; ", NULL, NULL, NULL);
    LogSql(result, "[BEGIN TRANSACTION]");
}


void DatabaseUtil::Bind(const unsigned int& index, const unsigned int& p)
{
    sqlite3_bind_int(m_stmt, index, p);
}


void DatabaseUtil::Bind(const unsigned int& index, const char* param)
{
    sqlite3_bind_text(m_stmt, index, param, -1, SQLITE_TRANSIENT);
}


int DatabaseUtil::CloseConnection()
{
    int status = sqlite3_close_v2(m_connection);

    if (status != SQLITE_OK) {
        LogSql(status, "[Connection close fail]");
    }

    std::cout << "[Connection closed] statuscode: " << status << std::endl;

    return status;
}


void DatabaseUtil::ClearStatement()
{
    if (m_stmt != nullptr) {
        // Clear prepared statement
        //sqlite3_clear_bindings(m_stmt);
        sqlite3_finalize(m_stmt);
        std::cout << "[Finalize statement]" << std::endl;
    }
}


void DatabaseUtil::CommitTransaction()
{
    //sqlite3_busy_timeout(m_connection, 3000);
    int result = sqlite3_exec(m_connection, "END TRANSACTION; ", NULL, NULL, NULL);
    LogSql(result, "[COMMIT TRANSACTION]");
    if (result != SQLITE_DONE && result != SQLITE_OK) {
        RollbackTransaction();
    }
}


int DatabaseUtil::ExecutesStatement()
{
    int result = sqlite3_step(m_stmt);
    if (result != SQLITE_ROW && result != SQLITE_DONE && result != SQLITE_OK) {
        LogSql(result, "Executes statement fail");
        RollbackTransaction();
        FinalizeStatement();
    }

    return result;
}


void DatabaseUtil::FinalizeStatement()
{
    sqlite3_finalize(m_stmt);
}


const char* DatabaseUtil::GetErrors()
{
    return sqlite3_errmsg(m_connection);
}


unsigned int DatabaseUtil::GetLastRowInsertId() const
{
    return sqlite3_last_insert_rowid(m_connection);
}


unsigned int DatabaseUtil::GetRowInt(const unsigned int& index)
{
    return sqlite3_column_int(m_stmt, index);
}

long unsigned int DatabaseUtil::GetRowLong(const unsigned int& index)
{
    std::stringstream strValue;
    strValue << sqlite3_column_text(m_stmt, index);

    long unsigned int long_int;
    strValue >> long_int;

    return long_int;
}

const char* DatabaseUtil::GetRowStr(const unsigned int& index)
{
    return reinterpret_cast<const char*>( sqlite3_column_text(m_stmt, index) );
}

double DatabaseUtil::GetRowDouble(const unsigned int& index)
{
    return sqlite3_column_double(m_stmt, index);
}

void DatabaseUtil::LogSql(const int& code_err, const char* message)
{
    switch (code_err) {
        case SQLITE_OK:
            std::cout << "[SQL OK] code: " << code_err << std::endl;
            break;
        case SQLITE_DONE:
            std::cout << message << " [SQL DONE] code: " << code_err << std::endl;
            break;
        case SQLITE_ERROR:
            std::cerr << "[Error] " << code_err << std::endl;
            break;
        case SQLITE_CONSTRAINT:
            std::cerr << "[Primary key duplicate] errorcode: " << code_err << std::endl;
            break;
        case SQLITE_BUSY:
            std::cerr << "[Database busy] err_code: " << code_err << std::endl;
            break;
        default:
            std::cerr << "[SQL log] " << message << " " << code_err << std::endl;
            std::cerr << GetErrors() << std::endl << std::endl;
    }
}

int DatabaseUtil::OpenConnection()
{
    const char* path = "bookmarks.db";

    int status = sqlite3_open(path, &m_connection);
    if (status != SQLITE_OK) {
        std::cerr << "[Cannot open database] errcode: " << status << std::endl;
    }

    //sqlite3_busy_timeout(m_connection, 3000);
    return status;
}


bool DatabaseUtil::PreparedStatement(const char* sql)
{
    int status = sqlite3_prepare_v2(
                         m_connection,   /* Database handle */
                         sql,            /* SQL statement, UTF-8 encoded */
                         -1,             /* Maximum length of zSql in bytes. */
                         &m_stmt,        /* OUT: Statement handle */
                         nullptr         /* OUT: Pointer to unused portion of zSql */
                 );

    if (status == SQLITE_OK) {
        std::cout << "[Prepared statement valid] status_code: " << status << std::endl;
        return true;
    }

    std::cerr << "[Prepared statement fail] errcode: " << status << std::endl;
    RollbackTransaction();
    FinalizeStatement();

    return false;
}


void DatabaseUtil::RollbackTransaction()
{
    std::cout << "[ROLLBACK TRANSACTION]" << std::endl;
    sqlite3_exec(m_connection, "ROLLBACK;", NULL, NULL, NULL);
}
