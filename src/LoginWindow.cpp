#include "LoginWindow.h"
#include <iostream>
#include "BookmarkRepository.h"
#include <bits/stdc++.h>
#include "CommonUtil.h"
#include "MainWindow.h"


LoginWindow::LoginWindow()
{
    set_title("Login");
    set_size_request(500, 300);
    set_position(Gtk::WIN_POS_CENTER);
    set_border_width(10);

    m_username_lbl.set_label("Username: ");
    m_username_entry.set_text("joni@gmail.com");
    m_password_lbl.set_label("Password: ");
    m_password_entry.set_text("aaa");
    m_password_entry.set_visibility(false);
    m_username_lbl.set_alignment(Gtk::ALIGN_END);
    m_btn_login.set_label("Login");
    m_btn_login.signal_clicked()
    .connect(sigc::mem_fun(*this, &LoginWindow::onLoginClick));

    m_grid.set_halign(Gtk::ALIGN_CENTER);
    m_grid.set_margin_top(10);
    m_grid.set_column_spacing(10);
    m_grid.set_row_spacing(7);
    m_grid.attach(m_username_lbl, 0, 0, 1, 1);
    m_grid.attach_next_to(m_username_entry, m_username_lbl, Gtk::POS_RIGHT, 1, 1);
    m_grid.attach_next_to(m_password_lbl, m_username_lbl, Gtk::POS_BOTTOM, 1, 1);
    m_grid.attach_next_to(m_password_entry, m_password_lbl, Gtk::POS_RIGHT, 1, 1);
    m_grid.attach_next_to(m_btn_login, m_password_entry, Gtk::POS_BOTTOM, 1, 1);
    add(m_grid);
    show_all_children();
}

LoginWindow::~LoginWindow()
{
    std::cout << "Login window destroyed" << std::endl;
}

void LoginWindow::onLoginClick()
{
    std::string username = CommonUtil::trim(m_username_entry.get_text());
    std::string psw = CommonUtil::trim(m_password_entry.get_text());

    if (!CommonUtil::validatesEmailFormat(username)) {
        std::cout << "Email is not valid" << std::endl;
        return;
    }

    BookmarkRepository repository;
    std::string messages;
    if ( repository.login(username, psw, messages) ) {
        m_username_entry.set_text("");
        m_password_entry.set_text("");
//        MainWindow main_window;
//        main_window.show();
        this->hide();
        std::cout << "Showing main window " << messages << std::endl;
    }

    std::cout << "Error message: " << messages << std::endl;
}
