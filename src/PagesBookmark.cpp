//#include <iostream>
//#include <gtkmm/button.h>
//#include <gtkmm/dialog.h>
#include "PagesBookmark.h"
#include "CommonUtil.h"
#include "BookmarkDelete.h"
#include "FormBookmark.h"
#include "FormTags.h"


PagesBookmark::PagesBookmark():
    m_BtnAdd("Create New"),
    m_CreatePopover(m_BtnAdd)
{
    set_border_width(10);
    set_row_spacing(5);

    m_SearchEntry.set_placeholder_text("Search...");
    m_SearchEntry.set_hexpand(false);
    m_TagCb.set_margin_right(5);
    m_SearchBox.pack_start(m_TagPopover, false, false, 0);
    m_SearchBox.pack_start(m_SearchEntry, false, false, 5);

    m_TagPopover.signal_toggled().connect(
            sigc::mem_fun(*this, &PagesBookmark::OnSearching)
    );
    m_SearchEntry.signal_activate().connect(
            sigc::mem_fun(*this, &PagesBookmark::OnSearching)
    );
    m_BtnAdd.signal_toggled().connect(
            sigc::mem_fun(*this, &PagesBookmark::OnClickAdd)
    );
    m_BtnDelete.signal_clicked().connect(
            sigc::mem_fun(*this, &PagesBookmark::OnClickDelete)
    );
    m_Paginate.m_NextBtn.signal_clicked().connect(
            sigc::mem_fun(*this, &PagesBookmark::OnClickNext)
    );
    m_Paginate.m_PrevBtn.signal_clicked().connect(
            sigc::mem_fun(*this, &PagesBookmark::OnClickPrev)
    );
    m_TagPopover.SignalOnRowActivated().connect(
            sigc::mem_fun(*this, &PagesBookmark::OnTagSelected)
    );

    //m_BtnBox.set_layout(Gtk::ButtonBoxStyle::BUTTONBOX_END);
    m_BtnBox.set_halign(Gtk::Align::ALIGN_END);
    m_BtnBox.set_spacing(5);
    m_BtnBox.add(m_BtnAdd);
    m_BtnBox.add(m_BtnDelete);
    m_BtnAdd.set_image_from_icon_name("document-new");
    m_BtnAdd.set_always_show_image(true);
    m_BtnDelete.set_image_from_icon_name("edit-delete");
    m_BtnDelete.set_always_show_image(true);
    m_BtnDelete.set_focus_on_click(false);

    m_ScrlledWindow.add(m_TvBookmark);
    m_ScrlledWindow.set_policy(Gtk::PolicyType::POLICY_AUTOMATIC, Gtk::PolicyType::POLICY_AUTOMATIC);
    m_ScrlledWindow.set_hexpand(true);
    m_ScrlledWindow.set_vexpand(true);

    // Create treemodel
    m_RefTreeModel = Gtk::ListStore::create(m_Columns);
    m_TvBookmark.set_model(m_RefTreeModel);

    //Add the TreeView's view columns:
    //This number will be shown with the default numeric formatting.
    m_TvBookmark.append_column(" ID", m_Columns.m_col_id);
    m_TvBookmark.append_column(" Title", m_Columns.m_col_title);
    m_TvBookmark.append_column(" Rating", m_Columns.m_col_rating);
    m_TvBookmark.append_column(" URL ID", m_Columns.m_col_link_id);
    m_TvBookmark.append_column(" URL", m_Columns.m_col_link_uri);
    //m_tvbookmark.get_column(0)->set_visible(false);
    //m_tvbookmark.get_column(2)->set_property("xalign", 1.00);
    m_TvBookmark.get_column(2)->set_alignment(1.00);


//    Gtk::CellRendererText renderer;
//    renderer.set_property("xalign", 1.00);
//    m_tvbookmark.get_column(1)->add_attribute(renderer, "", 1);
//    Gtk::TreeViewColumn cols("Tester", m_columns.m_col_link_id);
//    m_tvbookmark.append_column(cols);

    m_TvBookmark.signal_row_activated().connect( sigc::mem_fun(*this, &PagesBookmark::OnSelectTv) );

    attach(m_SearchBox, 0, 0, 1, 1);
    attach_next_to(m_BtnBox, m_SearchBox, Gtk::PositionType::POS_RIGHT, 1, 1);
    attach_next_to(m_ScrlledWindow, m_SearchBox, Gtk::PositionType::POS_BOTTOM, 2, 1);
    attach(m_Paginate, 0, 3, 2, 1);
    show_all();
    OnSearching();
    m_TagPopover.set_visible(true);
    ConfigureCreatePopover();
}

PagesBookmark::~PagesBookmark()
{}

void PagesBookmark::FillTvBookmarks(unsigned int pages = 0)
{
    m_RefTreeModel->clear();

    std::string searching = CommonUtil::appendFirstAndLast(m_SearchEntry.get_text(), "%");
    std::string output_messages;

    const std::vector<Bookmark>& data = repository.FindAll(searching, pages, this->m_TagId, output_messages);
    for ( const Bookmark& b :  data) {
        Gtk::TreeModel::Row row = *(m_RefTreeModel->append());
        row[m_Columns.m_col_id] = b.getId();
        row[m_Columns.m_col_title] = b.getTitle();
        row[m_Columns.m_col_rating] = b.getRating();
        row[m_Columns.m_col_link_id] = b.getLink().getIds();
        row[m_Columns.m_col_link_uri] = b.getLink().getUrls();
    }

    m_Paginate.SetTotalData(data.size());
}

void PagesBookmark::OnSearching()
{
    FillTvBookmarks();
}

void PagesBookmark::OnClickAdd()
{
    bool active = m_BtnAdd.get_active();
    m_CreatePopover.set_visible(active);
}


void PagesBookmark::OnClickDelete()
{
    const Glib::RefPtr<Gtk::TreeSelection>& selection = m_TvBookmark.get_selection();
    const Gtk::TreeModel::iterator& iter = selection->get_selected();

    if (iter == nullptr) {
        return;
    }

    Gtk::TreeModel::Row row = *iter;
    const unsigned int& bookmark_id = row[m_Columns.m_col_id];

    BookmarkDelete dialog(bookmark_id);
    int result = dialog.run();

    switch (result) {
        case(Gtk::RESPONSE_OK):
            OnSearching();
            break;
        case(Gtk::RESPONSE_CANCEL):
            break;
        default:
            dialog.close();
    }
}

void PagesBookmark::OnSelectTv(const Gtk::TreeModel::Path& path, Gtk::TreeViewColumn* column)
{
    const Gtk::TreeModel::iterator& iter = m_RefTreeModel->get_iter(path);
    if (iter) {
        Gtk::TreeModel::Row row = *iter;
        unsigned int ids = row[m_Columns.m_col_id];
        OpenBookmarkForm(ids);
    }
}

void PagesBookmark::OpenBookmarkForm(const unsigned int& ids)
{
    FormBookmark dialog(ids);
    dialog.set_default_size(200, 200);
    int result = dialog.run();

    switch(result) {
        case(Gtk::ResponseType::RESPONSE_OK):
            FillTvBookmarks(m_Paginate.GetCurrentPages() * m_Paginate.GetRowPerPages());
            break;
        case(Gtk::RESPONSE_CANCEL):
            break;
        case(Gtk::ResponseType::RESPONSE_DELETE_EVENT):
            break;
        default:
            break;
    }
}

void PagesBookmark::OnClickNext()
{
    FillTvBookmarks(m_Paginate.Next());
}

void PagesBookmark::OnClickPrev()
{
    FillTvBookmarks(m_Paginate.Prev());
}

void PagesBookmark::OnTagSelected(const bool& value_changes, const int& tag_id, const std::string& message)
{
    if (!value_changes) {
        return;
    }

    this->m_TagId = tag_id;
    FillTvBookmarks(0);
    m_TagPopover.set_active(false);
}

void PagesBookmark::ConfigureCreatePopover()
{
    m_CreatePopover.set_position(Gtk::PositionType::POS_BOTTOM);
    m_CreatePopover.set_visible(false);
    m_CreatePopover.set_modal(true);
    m_CreatePopover.signal_hide().connect(
        sigc::mem_fun(*this, &PagesBookmark::OnHideCreatePopover)
    );
    m_CreatePopover.set_size_request(200, -1);
    m_CreatePopover.set_border_width(5);

    m_CreateBookmark.set_label("Bookmark");
    m_CreateTag.set_label("Tag");
    m_CreateBookmark.set_focus_on_click(false);
    m_CreateTag.set_focus_on_click(false);
    m_CreateBookmark.signal_clicked().connect(
        sigc::mem_fun(*this, &PagesBookmark::OnClickItemBookmark)
    );
    m_CreateTag.signal_clicked().connect(
        sigc::mem_fun(*this, &PagesBookmark::OnClickItemTag)
    );

    //m_CreateBookmark.set_image_from_icon_name("user-bookmarks");
    m_CreateBookmark.set_always_show_image(true);
    m_CreatePopoverBox.set_margin_top(3);
    m_CreatePopoverBox.set_margin_bottom(3);
    m_CreatePopoverBox.add(m_CreateBookmark);
    m_CreatePopoverBox.set_orientation(Gtk::Orientation::ORIENTATION_VERTICAL);
    m_CreatePopoverBox.add(m_CreateTag);

    m_CreatePopover.add(m_CreatePopoverBox);
    m_CreatePopover.show_all_children();
}

void PagesBookmark::OnHideCreatePopover()
{
    m_BtnAdd.set_active(false);
}

void PagesBookmark::OnClickItemBookmark()
{
    OpenBookmarkForm(0);
}

void PagesBookmark::OnClickItemTag()
{
    FormTags dialog;
    int result = dialog.run();
}
