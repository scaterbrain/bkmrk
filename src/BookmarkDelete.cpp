#include "BookmarkDelete.h"
#include "BookmarkRepository.h"

BookmarkDelete::BookmarkDelete(const unsigned int& bookmarkId):
    m_bookmark_id(bookmarkId)
{
    set_title("Delete Confirmation");
    set_size_request(500, 100);

    m_content_label.set_label("Are you sure wants to delete?");
    m_btn_cancel.set_label("Cancel");
    m_btn_submit.set_label("Submit");
    // Signal
    m_btn_cancel.signal_clicked().connect(
        sigc::mem_fun(*this, &BookmarkDelete::onCancel)
    );
    m_btn_submit.signal_clicked().connect(
        sigc::mem_fun(*this, &BookmarkDelete::onSubmit)
    );

    m_content_box.set_border_width(10);
    m_content_box.add(m_content_label);
    m_btn_box.set_spacing(10);
    m_btn_box.set_border_width(10);
    m_btn_box.set_halign(Gtk::Align::ALIGN_END);
    m_btn_box.add(m_btn_submit);
    m_btn_box.add(m_btn_cancel);

    this->get_vbox()->add(m_content_box);
    this->get_vbox()->pack_end(m_btn_box, false, true, 0);
    this->get_vbox()->pack_end(m_separator, false, false, 0);
    show_all_children();
}

void BookmarkDelete::onSubmit()
{
    std::string message;
    BookmarkRepository repo;
    repo.Delete(m_bookmark_id, message);

    if (message.empty()) {
        this->response(Gtk::ResponseType::RESPONSE_OK);
    }
}

void BookmarkDelete::onCancel()
{
    this->response(Gtk::ResponseType::RESPONSE_CANCEL);
}


BookmarkDelete::~BookmarkDelete()
{
}
