#include "DeleteConfirmationDialog.h"


DeleteConfirmationDialog::DeleteConfirmationDialog(const char* message) :
    m_ExtraMessage(message),
    m_MessageLbl("Are you sure wants to delete?"),
    m_SubmitBtn("Yes"),
    m_CancelBtn("Cancel")
{
    set_default_size(400, -1);
    set_title("Delete Confirmation");
    ConfigureMain();
    ConfigureFooter();
    this->get_content_area()->set_border_width(0);
    this->get_content_area()->pack_start(m_BoxMain, false, false, 0);
    this->get_content_area()->pack_end(m_ActionBar, false, false, 0);
    show_all();
}

DeleteConfirmationDialog::~DeleteConfirmationDialog()
{
    //dtor
}

void DeleteConfirmationDialog::ConfigureMain() {
    m_Image.set_from_icon_name("dialog-warning", Gtk::IconSize(3));
    m_MessageLbl.set_alignment(Gtk::Align::ALIGN_START, Gtk::Align::ALIGN_START);
    m_ExtraMessageLbl.set_label(m_ExtraMessage);
    m_ExtraMessageLbl.set_alignment(Gtk::Align::ALIGN_START, Gtk::Align::ALIGN_START);
    m_ExtraMessageLbl.set_opacity(0.7);
    m_Grid.attach(m_Image, 0, 0, 1, 1);
    m_Grid.attach(m_MessageLbl, 1, 0, 1, 1);
    m_Grid.attach(m_ExtraMessageLbl, 1, 1, 1, 1);
    m_Grid.set_column_spacing(10);
    m_BoxMain.set_border_width(30);
    m_BoxMain.add(m_Grid);
}

void DeleteConfirmationDialog::ConfigureFooter() {
    m_SubmitBtn.set_size_request(120, -1);
    m_SubmitBtn.set_image_from_icon_name("edit-delete");
    m_SubmitBtn.set_always_show_image(true);
    m_SubmitBtn.signal_clicked().connect(sigc::mem_fun(*this, &DeleteConfirmationDialog::OnSubmitClicked));
    m_CancelBtn.set_size_request(100, -1);
    m_CancelBtn.signal_clicked().connect(sigc::mem_fun(*this, &DeleteConfirmationDialog::OnCancelClicked));
    m_ActionBar.pack_end(m_CancelBtn);
    m_ActionBar.pack_end(m_SubmitBtn);
}

void DeleteConfirmationDialog::OnSubmitClicked() {
    this->response(Gtk::ResponseType::RESPONSE_OK);
}

void DeleteConfirmationDialog::OnCancelClicked() {
    this->response(Gtk::ResponseType::RESPONSE_CANCEL);
}
