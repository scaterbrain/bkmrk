#include "MainWindow.h"
#include "Bookmark.h"
#include "Link.h"
#include "Session.h"


MainWindow::MainWindow() :
    m_PagesLogin(
        std::bind(&MainWindow::LoginCallback, this, std::placeholders::_1, std::placeholders::_2),
        m_InfoBar
    ),
    m_PagesRegister(m_InfoBar),
    m_HeaderBar(m_LogoutBtn)
{
    m_LogoutBtn.signal_clicked().connect(
        sigc::mem_fun(*this, &MainWindow::LogoutCallback)
    );
    set_size_request(600, 400);
    set_position(Gtk::WindowPosition::WIN_POS_CENTER);
    set_titlebar(m_HeaderBar);
    add(m_Grid);

    m_Stack.set_hexpand(true);
    m_Stack.set_vexpand(true);
    m_Stack.set_transition_type(Gtk::StackTransitionType::STACK_TRANSITION_TYPE_CROSSFADE);
    m_Stack.set_transition_duration(1000);
    m_Stack.add(m_PagesLogin, "Login", "Login");
    m_Stack.add(m_PagesRegister, "Register", "Register");
    m_StackSwitcher.set_stack(m_Stack);

    m_Stack.connect_property_changed("visible-child", sigc::mem_fun(*this, &MainWindow::OnStackChange));

    m_StackSwitcherWrapper.set_halign(Gtk::Align::ALIGN_CENTER);
    m_StackSwitcherWrapper.set_margin_top(10);
    m_StackSwitcherWrapper.set_margin_bottom(10);
    m_StackSwitcherWrapper.add(m_StackSwitcher);

    m_Grid.attach(m_InfoBar, 0, 0, 1, 1);
    m_Grid.attach(m_StackSwitcherWrapper, 0, 1, 1, 1);
    m_Grid.attach(m_Stack, 0, 2, 1, 1);
    show_all_children();
    //m_stack.set_visible_child("register");
    //m_stack.set_focus_on_click(false);
    m_InfoBar.hide();
}

MainWindow::~MainWindow()
{}

void MainWindow::LoginCallback( bool flag, std::string messages )
{
    if (!flag) {
        m_InfoBar.SetErrors(messages.c_str(), Gtk::MessageType::MESSAGE_ERROR);
        return;
    }

    if (m_PagesBookmarkPtr == nullptr) {
        m_PagesBookmarkPtr = std::make_unique<PagesBookmark>();
    }

    m_Stack.add(*m_PagesBookmarkPtr.get(), "Bookmark");
    m_Stack.set_visible_child("Bookmark");
    m_HeaderBar.ShowUserToolButton();
    m_Stack.remove(m_PagesLogin);
    m_Stack.remove(m_PagesRegister);
    m_StackSwitcherWrapper.set_visible(false);
    m_Stack.set_state(Gtk::StateType::STATE_FOCUSED);
}

void MainWindow::OnStackChange()
{
    set_title(m_Stack.get_visible_child_name());
    m_InfoBar.hide();
}

void MainWindow::LogoutCallback()
{
    Session::RemoveCurrentUser();
    m_Stack.remove(*m_PagesBookmarkPtr.get());
    m_PagesBookmarkPtr.reset();
    m_Stack.add(m_PagesLogin, "Login", "Login");
    m_Stack.add(m_PagesRegister, "Register", "Register");
    OnStackChange();

    m_StackSwitcherWrapper.set_visible(true);
    m_StackSwitcherWrapper.show_all_children();
}
