#include "FormTags.h"
#include "CommonUtil.h"
#include "DeleteConfirmationDialog.h"
//#include <gtkmm/entry.h>
//#include <gtkmm/spinbutton.h>

FormTags::FormTags():
    m_TagId(0),
    m_BtnSubmit("Create")
{
    Init();
    set_title("Create");
}

FormTags::FormTags(const scaterbrain::Tag& tag):
    m_TagId(tag.GetId()),
    m_BtnSubmit("Update")
{
    Init();
    set_title("Update");
    m_NameEntry.set_text(tag.GetName());
    m_RatingEntry.set_value(tag.GetRating());
}

FormTags::~FormTags()
{
    //dtor
}

void FormTags::Init()
{
    set_default_size(400, -1);
    this->get_content_area()->set_border_width(0);
    this->get_content_area()->pack_start(m_InfoBar, false, false, 0);

    m_NameLabel.set_label("Name:");
    m_NameEntry.set_hexpand(true);
    m_NameEntry.set_max_length(30);
    m_RatingEntry.set_increments(1, 1);
    m_RatingLabel.set_label("Rating:");
    m_RatingEntry.set_max_length(3);
    m_RatingEntry.set_range(0, 100);
    m_RatingEntry.set_hexpand(false);
    m_BtnCancel.set_label("Cancel");
    m_Grid.set_border_width(10);
    m_Grid.set_column_spacing(10);
    m_Grid.set_row_spacing(5);
    m_Grid.attach(m_NameLabel, 0, 0, 1, 1);
    m_Grid.attach(m_NameEntry, 1, 0, 1, 1);
    m_Grid.attach(m_RatingLabel, 0, 1, 1, 1);
    m_Grid.attach(m_RatingEntry, 1, 1, 1, 1);
    this->get_content_area()->pack_start(m_Grid, false, false, 0);

    m_ActionBar.pack_end(m_BtnCancel);
    m_ActionBar.pack_end(m_BtnSubmit);
    m_ActionBar.pack_start(m_BtnDelete);
    m_BtnDelete.set_image_from_icon_name("edit-delete");
    m_BtnSubmit.set_image_from_icon_name("document-new", Gtk::IconSize(1));
    m_BtnSubmit.set_always_show_image(true);
    m_BtnSubmit.set_focus_on_click(false);
    m_BtnCancel.signal_clicked().connect(sigc::mem_fun(*this, &FormTags::OnClickCancel));
    m_BtnSubmit.signal_clicked().connect(sigc::mem_fun(*this, &FormTags::OnClickSubmit));
    m_BtnDelete.signal_clicked().connect(sigc::mem_fun(*this, &FormTags::OnClickDelete));
    this->get_content_area()->pack_end(m_ActionBar, false, true, 0);

    show_all_children();

    // Hide components
    m_InfoBar.hide();
}

void FormTags::OnClickSubmit()
{
    m_BtnSubmit.grab_focus();
    m_BtnSubmit.set_sensitive(false);

    if (!CommonUtil::isLengthValid(CommonUtil::trim(m_NameEntry.get_text()), 3, 30)) {
        m_InfoBar.SetErrors("Name length is not valid", Gtk::MessageType::MESSAGE_ERROR);
        m_BtnSubmit.set_sensitive(true);
        return;
    }

    if (10 > m_RatingEntry.get_value_as_int()) {
        m_InfoBar.SetErrors("Rating is not valid", Gtk::MessageType::MESSAGE_ERROR);
        m_BtnSubmit.set_sensitive(true);
        return;
    }

    std::string error_message;
    scaterbrain::Tag tag(m_TagId, CommonUtil::trim(m_NameEntry.get_text()), m_RatingEntry.get_value());

    if (m_TagId != 0) {
        m_Repository.Update(tag, error_message);
    } else {
        m_Repository.Create(tag, error_message);
    }

    if (!error_message.empty()) {
        m_InfoBar.SetErrors(error_message.c_str(), Gtk::MessageType::MESSAGE_ERROR);
        m_BtnSubmit.set_sensitive(true);
        return;
    }

    this->response(Gtk::ResponseType::RESPONSE_OK);
}

void FormTags::OnClickCancel()
{
    this->response(Gtk::ResponseType::RESPONSE_CANCEL);
}

void FormTags::OnClickDelete()
{
    DeleteConfirmationDialog dialog("This is extra messages");
    int result = dialog.run();
    if (result == Gtk::ResponseType::RESPONSE_OK) {
        this->response(result);
    }
}


