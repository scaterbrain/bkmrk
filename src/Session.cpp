#include "Session.h"


Session::~Session() {
    //dtor
}

const unsigned int& Session::GetCurrentUser() {
    return m_CurrentUser;
}

void Session::SetCurrentUser(const unsigned int& users_id) {
    m_CurrentUser = users_id;
}

void Session::RemoveCurrentUser() {
    m_CurrentUser = 0;
}

unsigned int Session::m_CurrentUser = 0;
