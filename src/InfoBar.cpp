#include "InfoBar.h"
//#include <gtkmm/label.h>
//#include <gtkmm/image.h>


namespace scaterbrain {

    InfoBar::InfoBar() {
        set_show_close_button(true);
        m_MessageLabel.set_opacity(0.9);

        signal_response().connect(sigc::mem_fun(*this,
              &scaterbrain::InfoBar::OnInfoBarResponse) );

        auto infobar_container = dynamic_cast<Gtk::Container*>(get_content_area());
        if (infobar_container) {
            infobar_container->set_border_width(10);
            infobar_container->add(m_IconImage);
            infobar_container->add(m_MessageLabel);
        }
    }

    InfoBar::~InfoBar() {
    }

    void InfoBar::SetErrors(const char* message, const Gtk::MessageType& type) {
        m_IconImage.set_from_icon_name("dialog-error", Gtk::IconSize(1));
        set_message_type(type);
        m_MessageLabel.set_label(message);
        show();
    }

    void InfoBar::OnInfoBarResponse(const int& reponse) {
        hide();
    }
}
