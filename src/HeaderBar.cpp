#include "HeaderBar.h"
//#include <gtkmm/menutoolbutton.h>
//#include <gtkmm/comboboxtext.h>

HeaderBar::HeaderBar(Gtk::ModelButton& logout_btn):
    m_UserMenuPopover(m_UserMenuToggle),
    r_BtnLogout(logout_btn)
{
    set_subtitle("https://gitlab.com/scaterbrain/bkmrk");
    set_show_close_button(true);

    r_BtnLogout.signal_clicked().connect(
        sigc::mem_fun(*this, &HeaderBar::OnClickLogoutBtn)
    );

    ConfigureComboBox();
    //ConfigureTestMenu();
    ConfigureUserMenu();
}

HeaderBar::~HeaderBar()
{
    //dtor
}

void HeaderBar::OnClickUserMenuToggle()
{
    m_UserMenuPopover.set_visible(m_UserMenuToggle.get_active());
}

void HeaderBar::ConfigureTestMenu()
{
//    m_AboutMenuToolButton.show_all_children();
//    m_AboutMenuToolButton.set_label("About");
//    m_MenuItem.set_label("Menu Item");
//    m_Menu.set_size_request(300, 300);
//
//    m_Menu.add(m_MenuItem);
//    m_Menu.add(m_Separator);
//    m_AboutMenuToolButton.set_menu(m_Menu);
//
//    pack_end(m_AboutMenuToolButton);
}

void HeaderBar::ConfigureUserMenu()
{
    //m_MenuLogout.set_label("Logout 2");
    //m_UserMenuToggle.set_focus_on_click(false);
    m_BtnAbout.set_label("About");
    r_BtnLogout.set_label("Logout");
    m_BtnProfile.set_label("Profile");

    m_Box.set_orientation(Gtk::Orientation::ORIENTATION_VERTICAL);
    m_Box.set_spacing(4);
    m_Box.add(m_BtnAbout);

    m_UserMenuPopover.signal_hide().connect(
        sigc::mem_fun(*this, &HeaderBar::OnHideUserMenu)
    );

    m_UserMenuToggle.set_image_from_icon_name("stock_text_justify");
    m_UserMenuToggle.signal_clicked().connect(
        sigc::mem_fun(*this, &HeaderBar::OnClickUserMenuToggle)
    );

    m_UserMenuPopover.set_border_width(5);
    m_UserMenuPopover.set_size_request(200, -1);
    m_UserMenuPopover.add(m_Box);
    m_UserMenuPopover.show_all_children();

    pack_end(m_UserMenuToggle);
}

void HeaderBar::OnHideUserMenu()
{
    m_UserMenuToggle.set_active(false);
}

void HeaderBar::ShowUserToolButton()
{
    m_Box.add(m_Separator);
    m_Box.add(m_BtnProfile);
    m_Box.add(r_BtnLogout);
    m_Box.show_all_children();
}

void HeaderBar::OnClickLogoutBtn()
{
    m_Box.remove(m_Separator);
    m_Box.remove(m_BtnProfile);
    m_Box.remove(r_BtnLogout);
    m_UserMenuPopover.hide();
}

void HeaderBar::ConfigureComboBox()
{
    pack_start(m_ComboBox);
    m_ComboBox.append("Bookmark");
    m_ComboBox.append("Tag");
    m_ComboBox.set_popup_fixed_width(false);
    m_ComboBox.set_active(0);
}
