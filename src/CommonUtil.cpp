#include "CommonUtil.h"

#include <string>
#include <regex>


std::string CommonUtil::trim(std::string&& yourString)
{
    const char* typeOfWhitespace = " ";

    yourString.erase(yourString.find_last_not_of(typeOfWhitespace) + 1);
    yourString.erase(0, yourString.find_first_not_of(typeOfWhitespace));

    return yourString;
}

std::string CommonUtil::appendFirstAndLast(const std::string& yourString, const std::string& appendS)
{
    std::string strNew = yourString;

    strNew = trim(strNew.data());
    strNew.insert(0, appendS);
    strNew.insert(yourString.size() + 1, appendS);

    return strNew;
}

bool CommonUtil::isValidUrl(const std::string&& urls)
{
    const std::regex pattern(
            "((http|https)://)(www.)?[a-zA-Z0-9@:%._\\+~#?&//=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%._\\+~#?&//=]*)"
    );

    if (std::regex_match(urls, pattern)) {
        return true;
    }

    return false;
}


bool CommonUtil::isLengthValid(const std::string& yourString, const unsigned int& pmin, const unsigned int& pmax)
{
    if (yourString.length() >= pmin && yourString.length() <= pmax) {
        return true;
    }

    return false;
}

bool CommonUtil::isValidEmail(const std::string& email)
{
    const std::regex pattern("(\\w+)(\\.|_)?(\\w*)@(\\w+)(\\.(\\w+))+");
    return regex_match(email, pattern);
}

CommonUtil::CommonUtil()
{}

CommonUtil::~CommonUtil()
{}
