#include "Link.h"
//#include <string>
//#include <iostream>

Link::Link(const unsigned int& id):
    m_ids(id)
{
}

Link::Link(const std::string& urls)
    : m_urls(urls)
{
}

Link::Link(const unsigned int& ids, const std::string& urls)
    : m_ids(ids), m_urls(urls)
{
}

Link::~Link()
{
}

unsigned int Link::getIds() const {
    return m_ids;
}

std::string Link::getUrls() const {
    return m_urls;
}

void Link::setIds(const unsigned int& val) {
    m_ids = val;
}

void Link::setUrls(const std::string& val) {
    m_urls = val;
}
