#include "PagesLogin.h"
#include "CommonUtil.h"
#include "BookmarkRepository.h"
#include "Session.h"


PagesLogin::PagesLogin(std::function<void(bool, std::string)> callback, scaterbrain::InfoBar& infoBar) :
    m_callback(callback),
    r_InfoBar(infoBar)
{
    set_halign(Gtk::ALIGN_CENTER);
    set_margin_top(10);
    set_column_spacing(10);
    set_row_spacing(7);

    m_username_lbl.set_label("Username:");
    m_password_lbl.set_label("Password:");
    m_username_entry.set_text("joni@gmail.com");
    m_password_entry.set_text("aaa");
    m_password_entry.set_visibility(false);
    m_btn_login.set_label("Login");

    m_btn_login.signal_clicked().connect(sigc::mem_fun(*this, &PagesLogin::onLoginClick));

    attach(m_username_lbl, 0, 0, 1, 1);
    attach_next_to(m_username_entry, m_username_lbl, Gtk::POS_RIGHT, 1, 1);
    attach_next_to(m_password_lbl, m_username_lbl, Gtk::POS_BOTTOM, 1, 1);
    attach_next_to(m_password_entry, m_password_lbl, Gtk::POS_RIGHT, 1, 1);
    attach_next_to(m_btn_login, m_password_entry, Gtk::POS_BOTTOM, 1, 1);

    m_username_entry.grab_focus();
}

PagesLogin::~PagesLogin()
{}

void PagesLogin::onLoginClick()
{
    m_btn_login.set_sensitive(false);

    if (Session::GetCurrentUser() != 0) {
        m_btn_login.set_sensitive(true);
        return;
    }

    std::string username = CommonUtil::trim(m_username_entry.get_text());
    std::string psw = CommonUtil::trim(m_password_entry.get_text());

    if (!CommonUtil::isValidEmail(username)) {
        m_btn_login.set_sensitive(true);
        r_InfoBar.SetErrors("Username is not valid", Gtk::MessageType::MESSAGE_ERROR);
        return;
    }

    if (!CommonUtil::isLengthValid(psw, 3, 20)) {
        r_InfoBar.SetErrors("Password length is not valid", Gtk::MessageType::MESSAGE_ERROR);
        m_btn_login.set_sensitive(true);
        return;
    }

    //std::size_t h1 = std::hash<std::string>{}("MyString");

    BookmarkRepository repository;
    std::string error_messages;
    unsigned int users_id;

    if ( repository.Login(username, psw, users_id, error_messages) ) {
        m_username_entry.set_text("");
        m_password_entry.set_text("");
        Session::SetCurrentUser(users_id);;
        m_btn_login.set_sensitive(true);
        m_callback(true, "");
        return;
    }

    r_InfoBar.SetErrors("Credentials is not valid", Gtk::MessageType::MESSAGE_ERROR);
    m_callback(false, error_messages);
    m_btn_login.set_sensitive(true);
}

