#include "Paginate.h"

Paginate::Paginate():
    m_InfoLbl(""), m_TotalLbl("")
{
    pack_start(m_TotalLbl, false, true, 0);
    pack_end(m_NextBtn, false, false, 0);
    pack_end(m_InfoLbl, false, false, 20);
    pack_end(m_PrevBtn, false, false, 0);
    m_NextBtn.set_image_from_icon_name("go-next", Gtk::ICON_SIZE_BUTTON);
    m_PrevBtn.set_image_from_icon_name("go-previous", Gtk::ICON_SIZE_BUTTON);
    m_NextBtn.set_sensitive(false);
    m_PrevBtn.set_sensitive(false);
    hide();
}

Paginate::~Paginate()
{
    //dtor
}

void Paginate::SetTotalData(const unsigned int& total_data) {
    m_TotalData = total_data;
    m_PrevBtn.set_sensitive(false);

    m_TotalPages = m_TotalData / m_RowPerPages;
    if (GetTotalPages() % GetRowPerPages() > 0) {
        m_TotalPages++;
    }

    RefreshUi();
}

unsigned int Paginate::Next() {
    if (m_CurrentPage >= m_TotalPages) {
        return m_CurrentPage;
    }

    m_CurrentPage++;

    return GetCurrentPages() * m_RowPerPages;
}

unsigned int Paginate::Prev() {
    if (m_CurrentPage == 0) {
        return m_TotalPages;
    }

    m_CurrentPage--;

    return (GetCurrentPages() * m_RowPerPages);
}

unsigned int Paginate::GetCurrentPages() const {
    return m_CurrentPage;
}

unsigned int Paginate::GetTotalPages() const {
    return m_TotalPages;
}

unsigned int Paginate::GetRowPerPages() const {
    return m_RowPerPages;
}

void Paginate::RefreshUi() {
    m_InfoLbl.set_label("Page " +  std::to_string(GetCurrentPages()+1) + " of " + std::to_string(GetTotalPages()));
    m_TotalLbl.set_label("Row Perpages: " + std::to_string(m_RowPerPages) + ", Total data: " + std::to_string(m_TotalData));

    (GetTotalPages() == 0) ? m_InfoLbl.set_visible(false) : m_InfoLbl.set_visible(true);
    (m_CurrentPage == 0) ? m_PrevBtn.set_sensitive(false) :  m_PrevBtn.set_sensitive(true);
    ((GetCurrentPages() + 1) >= m_TotalPages) ? m_NextBtn.set_sensitive(false) :  m_NextBtn.set_sensitive(true);
}
