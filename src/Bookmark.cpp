#include "Bookmark.h"
#include "Link.h"


Bookmark::Bookmark(const std::string& title, const Link& link):
    m_title(title),
    m_link(link) {
    m_tags.reserve(7);
}

Bookmark::Bookmark(const unsigned int& ids, const std::string& title, const Link& link):
    m_ids(ids),
    m_title(title),
    m_link(link) {
    m_tags.reserve(7);
}

Bookmark::~Bookmark() {
    //dtor
}

unsigned int Bookmark::getId() const {
    return m_ids;
}

void Bookmark::setId(const unsigned int& val) {
    m_ids = val;
}

void Bookmark::setTitle(const std::string& val) {
    this->m_title = val;
}

std::string Bookmark::getTitle() const {
    return m_title;
};

void Bookmark::setRating(const double& rating) {
    m_rating = rating;
}

double Bookmark::getRating() const {
    return m_rating;
}

void Bookmark::setHidden(const bool& value) {
    m_hidden = value;
}

bool Bookmark::getHidden() const {
    return m_hidden;
};

void Bookmark::setDescription(const std::string& description) {
    m_description = description;
}

std::string Bookmark::getDescription() const {
    return m_description;
}

Link Bookmark::getLink() const {
    return m_link;
}

void Bookmark::setLink(const Link& link) {
    m_link = link;
}

void Bookmark::setTags(const std::vector<scaterbrain::Tag>& tags) {
    m_tags = tags;
}

std::vector<scaterbrain::Tag> Bookmark::getTags() const {
    return m_tags;
}

void Bookmark::addTags(const scaterbrain::Tag& t) {
    m_tags.push_back(t);
}
