#include "FormBookmark.h"
#include "CommonUtil.h"
#include "Tag.h"

#include <gtkmm/messagedialog.h>
#include <iostream>
//#include <future>
//#include <thread>
//#include <chrono>
//using namespace std::chrono_literals;
//#include <gtkmm/infobar.h>
//#include <gtkmm/treeview.h>
//#include <gtkmm/popover.h>
//#include <gtkmm/treeview.h>
//#include <gtkmm/liststore.h>
//#include <gtkmm/label.h>
//#include <gtkmm/dialog.h>
//#include <gtkmm/button.h>
//#include <gtkmm/togglebutton.h>
//#include <gtkmm/buttonbox.h>


FormBookmark::FormBookmark(const unsigned int& pbookmark):
    m_bookmark_id(pbookmark)
{
    m_tags.reserve(7);
    set_default_size(480, 400);
    set_size_request(480, 400);
    set_title("Create");

    m_refTreeModel = Gtk::ListStore::create(m_TagModelColumn);

    m_url_label.set_label("URL:");
    m_url_label.set_halign(Gtk::Align::ALIGN_START);
    m_url_entry.set_hexpand(true);
    m_url_entry.set_max_length(100);
    m_title_label.set_label("Title:");
    m_title_label.set_halign(Gtk::Align::ALIGN_START);
    m_title_entry.set_max_length(50);
    m_rating_label.set_halign(Gtk::Align::ALIGN_START);
    m_rating_label.set_label("Rating:");
    m_rating_entry.set_hexpand(true);
    m_rating_entry.set_range(0, 100);
    m_rating_entry.set_digits(1);
    m_rating_entry.set_draw_value();
    m_hidden_lbl.set_label("Hidden:");
    m_hidden_lbl.set_halign(Gtk::Align::ALIGN_START);

    m_grid.set_border_width(10);
    m_grid.set_row_spacing(10);
    m_grid.set_column_spacing(10);
    m_grid.attach(m_url_label, 0, 0, 1, 1);
    m_grid.attach_next_to(m_url_entry, m_url_label, Gtk::PositionType::POS_RIGHT, 1, 1);
    m_grid.attach_next_to(m_title_label, m_url_label, Gtk::PositionType::POS_BOTTOM, 1, 1);
    m_grid.attach_next_to(m_title_entry, m_url_entry, Gtk::PositionType::POS_BOTTOM, 1, 1);
    m_grid.attach_next_to(m_rating_label, m_title_label, Gtk::PositionType::POS_BOTTOM, 1, 1);
    m_grid.attach_next_to(m_rating_entry, m_title_entry, Gtk::PositionType::POS_BOTTOM, 1, 1);
    m_grid.attach_next_to(m_hidden_lbl, m_rating_label, Gtk::PositionType::POS_BOTTOM, 1, 1);
    m_grid.attach_next_to(m_hidden_cb, m_rating_entry, Gtk::PositionType::POS_BOTTOM, 1, 1);
    m_grid.attach_next_to(m_separator2, m_hidden_cb, Gtk::PositionType::POS_BOTTOM, 1, 1);
    m_grid.attach_next_to(m_tab, m_separator2, Gtk::PositionType::POS_BOTTOM, 1, 1);

    // Tabs section
    configureTabs();

    // Footer section
    m_footer_box.set_spacing(10);
    m_footer_box.set_border_width(10);
    m_footer_box.set_halign(Gtk::Align::ALIGN_END);
    m_footer_box.add(m_btn_submit);
    m_footer_box.add(m_btn_cancel);
    m_btn_cancel.set_label("Cancel");
    m_btn_submit.set_label("Create");
    m_btn_submit.set_image_from_icon_name("document-new");
    m_btn_submit.set_always_show_image(true);
    m_btn_submit.set_focus_on_click(false);

    // Main Layout
    set_border_width(0);
    get_content_area()->set_border_width(0);
    this->get_content_area()->add(m_Infobar);
    this->get_content_area()->add(m_grid);
    this->get_content_area()->pack_end(m_footer_box, false, true, 0);
    this->get_content_area()->pack_end(m_separator, false, false, 0);

    // Signal handlers
    m_btn_cancel.signal_clicked().connect(
        sigc::mem_fun(*this, &FormBookmark::onCancelForm)
    );
    m_btn_submit.signal_clicked().connect(
        sigc::mem_fun(*this, &FormBookmark::onSubmitForm)
    );
    this->signal_show().connect(
        sigc::mem_fun(*this, &FormBookmark::onShowForm)
    );
    m_btn_delete_current_tags.signal_clicked().connect(
        sigc::mem_fun(*this, &FormBookmark::onClickRemoveTags)
    );
    m_refTreeModel->signal_row_deleted().connect(
        sigc::mem_fun(*this, &FormBookmark::onTvCurrentTagsRowRemoved)
    );

    if (isEditMode()) {
        set_title("Edit Form");
        m_btn_submit.set_label("Update");
    }

    // SHOW WIDGET
    show_all_children();

    // Hidden widget
    m_Infobar.hide();

    if (!isEditMode()) {
        return;
    }

    if (auto bm = repository.FindById(m_bookmark_id)) {
        m_title_entry.set_text(bm->getTitle());
        m_url_entry.set_text(bm->getLink().getUrls());
        m_rating_entry.set_value(bm->getRating());
        m_hidden_cb.set_active(bm->getHidden());
        m_desc_buffer->set_text(bm->getDescription());
        m_desc_entry.set_buffer(m_desc_buffer);

        for (const scaterbrain::Tag& t : bm->getTags())
            m_tags.emplace_back(t);

        if (!m_tags.empty())
            fillTags();

        return;
    }

    m_btn_submit.set_sensitive(false);
    Gtk::MessageDialog dialog(*this, "Errors");
    dialog.set_secondary_text("Bookmark not found");
    dialog.run();
    this->close();

    // Sample dummy data
    //m_title_entry.set_text("New Bookmark");
    //m_url_entry.set_text("https://www.twitch.com");
    //m_rating_entry.set_value(30);
    //m_hidden_cb.set_active(true);
    //m_desc_buffer->set_text("AaaaAAAAAAaaaAAaaa \nSSSSSssssSssSSSSs");
    //m_desc_entry.set_buffer(m_desc_buffer);
}

FormBookmark::~FormBookmark()
{
    //dtor
}

const bool FormBookmark::isEditMode() const
{
    return (m_bookmark_id == 0) ? false : true;
}

void FormBookmark::onCancelForm()
{
    m_btn_cancel.set_sensitive(false);
    this->response(Gtk::ResponseType::RESPONSE_CANCEL);
}


bool FormBookmark::validatesForm(std::string& message)
{
    if ( m_url_entry.get_text_length() < 10 || m_url_entry.get_text_length() > 100) {
        m_url_entry.grab_focus();
        message = "Url length valid";
        return false;
    } else if ( !CommonUtil::isValidUrl(CommonUtil::trim(m_url_entry.get_text())) ) {
        m_url_entry.grab_focus();
        message = "Url not valid";
        return false;
    } else if ( m_title_entry.get_text_length() < 4 || m_title_entry.get_text_length() > 50) {
        m_title_entry.grab_focus();
        message = "Title minimum is 4 and max is 50";
        return false;
    } else if (10 > m_rating_entry.get_value()) {
        message = "Rating is not valid";
        m_rating_entry.grab_focus();
        return false;
    }

    return true;
}


static void Prints(const std::vector<scaterbrain::Tag>& tags, const char* title)
{
    std::cout << std::endl << title << ", size: " << tags.size() << std::endl;
    for (const scaterbrain::Tag& t : tags) {
        std::cout << "ID: " << t.GetId() << ", Name: " << t.GetName() << std::endl;
    }
}


void FormBookmark::onSubmitForm()
{
    m_btn_submit.set_sensitive(false);
    std::string message;
    if (!validatesForm(message)) {
        Gtk::MessageDialog dialog(*this, "Message", false,
                                  Gtk::MessageType::MESSAGE_WARNING, Gtk::ButtonsType::BUTTONS_CLOSE);
        dialog.set_secondary_text(message);
        dialog.set_default_size(400, 0);
        dialog.run();
        m_btn_submit.set_sensitive(true);
        return;
    }

    std::vector<scaterbrain::Tag> newTags;

    for (const auto& row : m_refTreeModel->children()) {
        unsigned int id = row[m_TagModelColumn.col_id];
        if (!isNewTags(id)) {
            std::cout << "Duplicates" << std::endl;
            continue;
        }
        scaterbrain::Tag tag(id);
        newTags.push_back(tag);
    }

    Prints(newTags, "New Tags:");

    std::vector<scaterbrain::Tag> deleteTags;

    for (const scaterbrain::Tag& t : m_tags) {
        if (isDeleteTags(t.GetId())) {
            deleteTags.push_back(t);
        }
    }

    Prints(deleteTags, "Delete Tags:");

    Link l(CommonUtil::trim(m_url_entry.get_text()));

    Bookmark b(CommonUtil::trim(m_title_entry.get_text()), l);
    b.setRating(m_rating_entry.get_value());
    b.setHidden(m_hidden_cb.get_active());
    m_desc_buffer = m_desc_entry.get_buffer();
    b.setDescription(m_desc_buffer->get_text(true));
    m_desc_buffer.reset();
    b.setTags(newTags);

    //BookmarkRepository repo;
    std::string outputMessages;

    if (isEditMode()) {
        b.setId(m_bookmark_id);
        repository.Update(b, deleteTags, outputMessages);
        if (outputMessages.empty()) {
            this->response(Gtk::ResponseType::RESPONSE_OK);
        }
    } else {
        repository.Create(b, outputMessages);
        if (outputMessages.empty()) {
            this->response(Gtk::ResponseType::RESPONSE_OK);
        }
    }

    m_Infobar.SetErrors(outputMessages.c_str(), Gtk::MESSAGE_ERROR);

    m_btn_submit.set_sensitive(true);
}

void FormBookmark::onShowForm()
{
    std::cout << "Focus" << std::endl;
}

void FormBookmark::fillTags()
{
    if (m_tags.empty()) {
        return;
    }

    m_refTreeModel->clear();

    for (const scaterbrain::Tag& t : m_tags) {
        addTagsPreview(t);
        std::cout << "Adding current row tags: " << t.GetName() << std::endl;
    }
}

void FormBookmark::addTagsPreview(const scaterbrain::Tag& t)
{
    Gtk::TreeModel::Row row = *(m_refTreeModel->append());
    row[m_TagModelColumn.col_id] = t.GetId();
    row[m_TagModelColumn.col_name] = t.GetName();
}

void FormBookmark::onClickRemoveTags()
{
    const Glib::RefPtr<Gtk::TreeSelection>& selection = m_tv_current_tags.get_selection();
    const Gtk::TreeModel::iterator& iter = selection->get_selected();

    if (iter == nullptr) {
        return;
    }

    Gtk::TreeModel::Row row = *iter;
    const unsigned int& tag_id = row[m_TagModelColumn.col_id];
    std::cout << "Remove clicked: " << tag_id << std::endl;

    const std::vector<Gtk::TreePath>& row2 = selection->get_selected_rows();
    for (const Gtk::TreePath& a : row2) {
        std::cout << a << std::endl;
    }

    m_refTreeModel->erase(iter);
}

void FormBookmark::onTvCurrentTagsRowRemoved(const Gtk::TreeModel::Path& path)
{
    std::cout << "removed" << path << std::endl;
}

bool FormBookmark::isNewTags(const unsigned int& pid)
{
    if (m_tags.empty()) {
        return true;
    }

    for (const scaterbrain::Tag& t : m_tags) {
        if (pid == t.GetId()) {
            return false;
        }
    }

    return true;
}

bool FormBookmark::isDeleteTags(const unsigned int& pid)
{
    const auto& children = m_refTreeModel->children();

    for (auto iter = children.begin(), end = children.end(); iter != end; ++iter) {
        const auto& row = *iter;
        const unsigned int& id = row[m_TagModelColumn.col_id];
        if (id == pid) {
            return false;
        }
    }

    return true;
}

bool FormBookmark::isAlreadyExist(const unsigned int& pid)
{
    for (const auto& row : m_refTreeModel->children()) {
        const unsigned int& id = row[m_TagModelColumn.col_id];
        if (id == pid) {
            std::cout << "Duplicates" << std::endl;
            return true;
        }
    }

    return false;
}

void FormBookmark::configureTabs()
{
    // Tab 1 Description
    m_desc_scrl.add(m_desc_entry);
    m_desc_scrl.set_vexpand(true);
    m_desc_buffer = Gtk::TextBuffer::create();
    m_desc_entry.set_border_width(10);

    // TAB 2 Current tags
    m_tv_current_tags.set_model(m_refTreeModel);
    m_tv_current_tags.append_column(" ID", m_TagModelColumn.col_id);
    m_tv_current_tags.append_column(" Title", m_TagModelColumn.col_name);
    m_tv_current_tags.set_vexpand(true);
    m_tv_current_tags.set_hexpand(true);
    m_scrl_tv_current_tags.add(m_tv_current_tags);
    m_box_current_tags.add(m_scrl_tv_current_tags);

    m_tool_current_tags.set_orientation(Gtk::Orientation::ORIENTATION_VERTICAL);
    m_tool_current_tags.set_spacing(3);
    m_tool_current_tags.set_border_width(5);
    m_tool_current_tags.set_hexpand(false);

    m_SearchTagPopover.ShowDescription(false);
    m_SearchTagPopover.ShowUnsetButton(false);
    m_SearchTagPopover.SetIcon("list-add");
    m_SearchTagPopover.SignalOnRowActivated().connect(
        sigc::mem_fun(*this, &FormBookmark::OnTagPopoverSelected)
    );

    m_btn_delete_current_tags.set_hexpand(false);
    m_btn_delete_current_tags.set_image_from_icon_name("list-remove", Gtk::IconSize(1));

    m_box_current_tags.add(m_tool_current_tags);
    m_tool_current_tags.add(m_SearchTagPopover);
    m_tool_current_tags.add(m_btn_delete_current_tags);

    // Fill Tabs
    m_tab.append_page(m_desc_scrl, "Description");
    m_tab.append_page(m_box_current_tags, "Tags");
}

void FormBookmark::OnTagPopoverSelected(const bool& a, const int& tag_id, const std::string& tag_name)
{
    if (!isAlreadyExist(tag_id)) {
        scaterbrain::Tag t(tag_id, tag_name);
        addTagsPreview(t);
    }

    m_SearchTagPopover.set_active(false);
}

//static void testing() {
//    std::future<bool> future = std::async(std::launch::async, [](){
//        std::this_thread::sleep_for(2s);
//        return true;
//    });
//
//    std::cout << "waiting...\n";
//    std::future_status status;
//    do {
//        status = future.wait_for(1s);
//        switch( status) {
//            case std::future_status::deferred: std::cout << "deferred\n"; break;
//            case std::future_status::timeout: std::cout << "timeout\n"; break;
//            case std::future_status::ready: std::cout << "ready!\n"; break;
//        }
//    } while (status != std::future_status::ready);
//
//    std::cout << "result is " << future.get() << '\n';
//    std::cout << "Submit" << std::endl;
//}
