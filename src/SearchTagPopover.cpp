#include "SearchTagPopover.h"
#include "BookmarkRepository.h"
#include "CommonUtil.h"
#include "FormTags.h"
//#include <gtkmm/menuitem.h>


SearchTagPopover::SearchTagPopover():
    m_Popover(*this),
    m_UnsetBtn("Unset"),
    m_ShowDescription(true)
{
    this->set_image_from_icon_name("go-down", Gtk::IconSize(0.1));
    this->set_image_position(Gtk::PositionType::POS_RIGHT);
    this->set_always_show_image(true);
    this->set_focus_on_click(false);

    this->signal_clicked().connect(
            sigc::mem_fun(*this, &SearchTagPopover::OnClick)
    );
    m_SearchEntry.signal_activate().connect(
            sigc::mem_fun(*this, &SearchTagPopover::OnSearch)
    );
    m_IconView.signal_item_activated().connect(
            sigc::mem_fun(*this, &SearchTagPopover::OnRowActivated)
    );
    m_UnsetBtn.signal_clicked().connect(
            sigc::mem_fun(*this, &SearchTagPopover::OnClickUnset)
    );
    m_Popover.signal_hide().connect(
            sigc::mem_fun(*this, &SearchTagPopover::OnHidePopover)
    );

    m_Popover.set_visible(false);
    m_Popover.set_border_width(5);
    m_Popover.set_size_request(300, 300);
    m_Popover.add(m_Grid);
    m_Grid.set_row_spacing(5);
    m_Grid.attach(m_SearchEntry, 0, 0, 1, 1);
    m_Grid.attach(m_ScrolledWindow, 0, 1, 1, 1);
    m_Grid.attach(m_UnsetBtn, 0, 2, 1, 1);
    m_Popover.show_all_children();

    m_ScrolledWindow.add(m_IconView);
    m_ScrolledWindow.show_all_children();
    m_IconView.set_hexpand(true);
    m_IconView.set_vexpand(true);

    // Create tree model
    m_refListModel = Gtk::ListStore::create(m_TagModelColumn);
    m_IconView.set_model(m_refListModel);
    m_IconView.set_markup_column(m_TagModelColumn.col_name);
    m_IconView.set_pixbuf_column(m_TagModelColumn.col_pixbuf);

    // Fill popup edit
    auto editItem = Gtk::make_managed<Gtk::MenuItem>("_Edit", true);
    editItem->signal_activate().connect(
            sigc::mem_fun(*this, &SearchTagPopover::OnClickEdit)
    );
    m_MenuEditPopup.append(*editItem);
    m_MenuEditPopup.accelerate(m_IconView);
    m_MenuEditPopup.show_all();
    m_IconView.signal_button_press_event().connect(
            sigc::mem_fun(*this, &SearchTagPopover::OnRightClickIconView)
    );

    FillTreeView();
    m_UnsetBtn.set_visible(false);
}

SearchTagPopover::~SearchTagPopover() {}

void SearchTagPopover::OnClick()
{
    m_Popover.set_visible(get_active());
}

void SearchTagPopover::OnSearch()
{
    FillTreeView();
}

void SearchTagPopover::AddRow(const scaterbrain::Tag& tag)
{
    Gtk::TreeModel::Row row = *(m_refListModel->append());
    row[m_TagModelColumn.col_id] = tag.GetId();
    row[m_TagModelColumn.col_name] = tag.GetName();
    row[m_TagModelColumn.col_pixbuf] = Gdk::Pixbuf::create_from_file("asset/tag.svg", 15, 15);
    row[m_TagModelColumn.col_rating] = tag.GetRating();
}

void SearchTagPopover::FillTreeView()
{
    m_refListModel->clear();
    BookmarkRepository repository;
    for (const scaterbrain::Tag& t : repository.FindAll( CommonUtil::appendFirstAndLast(m_SearchEntry.get_text(), "%"), 10) ) {
        AddRow(t);
    }
}

void SearchTagPopover::OnRowActivated(const Gtk::TreeModel::Path& path)
{
    const Gtk::TreeModel::iterator& iter = m_refListModel->get_iter(path);
    Gtk::TreeModel::Row row = *iter;

    const unsigned int id = row[m_TagModelColumn.col_id];
    const Glib::ustring name = row[m_TagModelColumn.col_name];

    if (m_ShowDescription) {
        this->set_label(name + "  ");
    }

    m_SignalOnRowActivated.emit(true, id, name);
    m_UnsetBtn.set_visible(true);
}

void SearchTagPopover::OnClickUnset()
{
    this->set_label("");
    m_SignalOnRowActivated.emit(true, 0, "");
    m_UnsetBtn.set_visible(false);
}

SearchTagPopover::type_signal_on_row_activated SearchTagPopover::SignalOnRowActivated()
{
    return m_SignalOnRowActivated;
}

void SearchTagPopover::ShowDescription(const bool& visible)
{
    this->m_ShowDescription = visible;
}

void SearchTagPopover::ShowUnsetButton(const bool& visible)
{
    this->m_UnsetBtn.set_visible(false);
}

void SearchTagPopover::SetIcon(const std::string& icon_name)
{
    this->set_image_from_icon_name(icon_name, Gtk::IconSize(0.1));
}

void SearchTagPopover::OnHidePopover()
{
    this->set_active(false);
}

bool SearchTagPopover::OnRightClickIconView(GdkEventButton* button_event)
{
    if (m_IconView.get_selected_items().empty()) {
        return false;
    }

    bool return_value = true;

    //Call base class, to allow normal handling,
    //such as allowing the row to be selected by the right-click:
    //return_value = TreeView::on_button_press_event(button_event);

    if( (button_event->type == GDK_BUTTON_PRESS) && (button_event->button == 3) ) {
        m_MenuEditPopup.popup_at_pointer((GdkEvent*)button_event);
    }

    return return_value;
}

void SearchTagPopover::OnClickEdit()
{
    m_Popover.set_modal(false);

    typedef std::vector<Gtk::TreeModel::Path> type_vec_paths;
    type_vec_paths selected = m_IconView.get_selected_items();

    if (!selected.empty()) {
        const Gtk::TreeModel::Path& path = *selected.begin();
        const Gtk::TreeModel::iterator& iter = m_refListModel->get_iter(path);
        const Gtk::TreeModel::Row& row = *iter;

        const unsigned int& tagId = row[m_TagModelColumn.col_id];
        const Glib::ustring& tagName = row[m_TagModelColumn.col_name];
        const double& tagRating = row[m_TagModelColumn.col_rating];
        const scaterbrain::Tag tag(tagId, tagName, tagRating);

        FormTags dialog(tag);
        int result = dialog.run();

        if (result == Gtk::ResponseType::RESPONSE_OK) {
            FillTreeView();
        }
    }

    m_Popover.set_modal(true);
}
