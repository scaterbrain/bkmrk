#include "BookmarkRepository.h"
//#include <sqlite3.h>
//#include <tuple>
//#include <regex>
//#include <exception>
//#include <stdexcept>
#include <iostream>
#include "Link.h"
#include "DatabaseUtil.h"
#include "Session.h"
//#include <functional>


BookmarkRepository::BookmarkRepository()
{
    std::cout << "Bookmark Repository initialized" << std::endl;
}

BookmarkRepository::~BookmarkRepository()
{
    std::cout << "Bookmark Repository destroyed" << std::endl;
}

void BookmarkRepository::Create(const Bookmark& b, std::string& outputMessages)
{
    DatabaseUtil& db = DatabaseUtil::GetInstance();

    db.OpenConnection();

    db.BeginTrasaction();

    unsigned int linkId = FindOrCreateLink(b.getLink(), outputMessages);

    if (linkId == 0) {
        return;
    }

    unsigned int bookmarkId = InsertBookmark(b, linkId, outputMessages);

    if (bookmarkId == 0) {
        return;
    }

    if (!b.getTags().empty() && bookmarkId != 0) {
        for (const scaterbrain::Tag& t : b.getTags()) {
            std::cout << "Temp ID: " << bookmarkId << ", TagID: " << t.GetId() << std::endl;
            InsertBookmarkTags(t.GetId(), bookmarkId, outputMessages);
        }
    }

    db.CommitTransaction();

    db.CloseConnection();
}

void BookmarkRepository::Create(const scaterbrain::Tag& t, std::string& outputMessages)
{
    DatabaseUtil& db = DatabaseUtil::GetInstance();

    db.OpenConnection();

    db.BeginTrasaction();

    const char* sql = "INSERT INTO TAGS(Name, Users_ID, Rating) values(?,?,?); ";
    const unsigned int& user_id = Session::GetCurrentUser();

    if (db.PreparedStatement(sql)) {
        db.Bind(1, t.GetName().c_str());
        db.Bind(2, user_id);
        db.Bind(3, t.GetRating());

        int status = db.ExecutesStatement();

        if (status == SQLITE_DONE) {
            db.FinalizeStatement();
            db.CommitTransaction();
            db.CloseConnection();
            return;
        }

        if (status == SQLITE_CONSTRAINT)
            outputMessages.append("Tag Already exists");
        else
            outputMessages.append("Tag Failed to create");
    }

    db.CloseConnection();
}

unsigned int BookmarkRepository::CountAllBookmark(const char* psearch)
{
    const char* sql = "SELECT COUNT(*) AS total_data from Bookmarks WHERE Users_ID=? AND Title like ?;";
    DatabaseUtil& db = DatabaseUtil::GetInstance();
    unsigned int total = 0;
    const unsigned int& user_id = Session::GetCurrentUser();

    if ( db.PreparedStatement(sql) ) {
        db.Bind(1, user_id);
        db.Bind(2, psearch);

        if (db.ExecutesStatement() == SQLITE_ROW) {
            total = db.GetRowInt(0);
            db.FinalizeStatement();
            return total;
        }
    }

    return total;
}

void BookmarkRepository::Delete(const unsigned int& bookmarkId, std::string& outputMessages)
{
    DatabaseUtil& db = DatabaseUtil::GetInstance();

    std::vector<scaterbrain::Tag> tags = GetTags(bookmarkId);

    db.OpenConnection();

    db.BeginTrasaction();

    if (DeleteBookmarkTags(bookmarkId, outputMessages)) {
        DeleteBookmark(bookmarkId, outputMessages);
    }

    db.CommitTransaction();

    db.CloseConnection();
}

void BookmarkRepository::Delete(const scaterbrain::Tag& tag, std::string& outputMessages)
{
//    DatabaseUtil& db = DatabaseUtil::GetInstance();
//
//    db.OpenConnection();
//
//    db.BeginTrasaction();
//
//    const char* sql = "DELETE FROM Tags WHERE ID=?; ";
//
//
//
//    db.RollbackTransaction();
//
//    db.CloseConnection();
}

bool BookmarkRepository::DeleteBookmark(const unsigned int& bookmarkId, std::string& outputMessages)
{
    DatabaseUtil& db = DatabaseUtil::GetInstance();
    const char* sql = "DELETE From Bookmarks where ID=?;";

    if ( db.PreparedStatement(sql) ) {
        db.Bind(1, bookmarkId);

        int status = db.ExecutesStatement();
        db.FinalizeStatement();

        if (status == SQLITE_DONE) {
            return true;
        }

        outputMessages = db.GetErrors();
    }

    return false;
}

bool BookmarkRepository::DeleteBookmarkTags(const unsigned int& bookmarkId, std::string& outputMessages)
{
    const char* sql = "DELETE from Tags_Bookmarks WHERE Bookmarks_ID=?;";
    DatabaseUtil& db = DatabaseUtil::GetInstance();

    if ( db.PreparedStatement(sql) ) {
        db.Bind(1, bookmarkId);

        int status = db.ExecutesStatement();
        db.FinalizeStatement();

        if (status == SQLITE_DONE) {
            return true;
        }

        outputMessages = db.GetErrors();
    }

    return false;
}

bool BookmarkRepository::DeleteBookmarkTags(const unsigned int& bookmarkId, const unsigned int& tagsId, std::string& outputMessages)
{
    const char* sql = "DELETE from Tags_Bookmarks WHERE Bookmarks_ID=? AND Tags_ID=?;";
    DatabaseUtil& db = DatabaseUtil::GetInstance();

    if ( db.PreparedStatement(sql) ) {
        db.Bind(1, bookmarkId);
        db.Bind(2, tagsId);

        int status = db.ExecutesStatement();
        db.FinalizeStatement();

        if (status == SQLITE_DONE) {
            return true;
        }

        outputMessages.append("Removing tag failed ");
    }

    return false;
}

std::optional<Bookmark> BookmarkRepository::FindById(const unsigned int& ids)
{

    const char* sql = "Select Bookmarks.ID as B_ID, Links_ID, Title, Hidden, Rating, Links.Uri as L_Uri, Description "
                      "from Bookmarks LEFT JOIN Links ON Bookmarks.Links_ID = Links.ID "
                      "where B_ID=?;";

    DatabaseUtil& db = DatabaseUtil::GetInstance();

    db.OpenConnection();

    if ( db.PreparedStatement(sql) ) {
        db.Bind(1, ids);
        if ( db.ExecutesStatement() == SQLITE_ROW ) {
            std::cout << "[Bookmarks Find by ID] - " << ids << std::endl;
            Link link(db.GetRowInt(1), db.GetRowStr(5));
            Bookmark bm(db.GetRowInt(0), db.GetRowStr(2), link);
            bm.setRating(db.GetRowInt(4));
            bm.setHidden(db.GetRowInt(3));
            bm.setDescription(db.GetRowStr(6));

            // IMPORTANT clear statements before calling another querys
            db.FinalizeStatement();

            bm.setTags(GetTags(ids));
            db.CloseConnection();
            return bm;
        }
    }

    db.CloseConnection();

    return std::nullopt;
}

std::vector<scaterbrain::Tag> BookmarkRepository::FindAll(const std::string& psearch, const unsigned short& limit)
{
    std::vector<scaterbrain::Tag> tags;
    tags.reserve(limit);

    const char* sql = "Select ID, Name, Rating from Tags where Users_ID=? AND Name like ? LIMIT ?;";
    const unsigned int user_ids = Session::GetCurrentUser();
    DatabaseUtil& db = DatabaseUtil::GetInstance();

    db.OpenConnection();

    if ( db.PreparedStatement(sql) ) {
        db.Bind(1, user_ids);
        db.Bind(2, psearch.c_str());
        db.Bind(3, limit);

        while ( db.ExecutesStatement() == SQLITE_ROW ) {
            scaterbrain::Tag tag(db.GetRowInt(0), db.GetRowStr(1), db.GetRowDouble(2));
            tags.push_back(tag);
        }

        db.FinalizeStatement();
    } else {
        std::cout << "[Failed Find All]" << std::endl;
    }

    db.CloseConnection();

    return tags;
}

std::vector<Bookmark> BookmarkRepository::FindAll(const std::string& psearch, const unsigned int& offset, const unsigned int& ptags, std::string& outputMessages)
{
    std::cout << "[Find All Bookmarks]" << std::endl;
    std::vector<Bookmark> bookmarks;
    bookmarks.reserve(20);
    DatabaseUtil& db = DatabaseUtil::GetInstance();
    const unsigned int user_id = Session::GetCurrentUser();

    std::string sql;

    if (ptags == 0) {
        sql =
            "SELECT Bookmarks.ID as B_ID, Links_ID, Title, Hidden, Rating, Links.Uri as L_Uri "
            "FROM Bookmarks "
            "LEFT JOIN Links ON Bookmarks.Links_ID = Links.ID "
            "where Users_ID=? AND (Title like ? or L_Uri like ?) LIMIT 10 OFFSET ?; ";
    } else {
        sql =
            "SELECT Bookmarks.ID as B_ID, Links_ID, Title, Hidden, Rating, Links.Uri as L_Uri "
            "FROM Bookmarks "
            "LEFT JOIN Links ON Bookmarks.Links_ID = Links.ID "
            "LEFT JOIN Tags_Bookmarks on B_ID = Tags_Bookmarks.Bookmarks_ID "
            "WHERE Users_ID=? AND (Title like ? or L_Uri like ?) AND Tags_Bookmarks.Tags_ID=? GROUP by B_ID LIMIT 10 OFFSET ?; ";
    }

    db.OpenConnection();

    if (db.PreparedStatement(sql.c_str())) {
        db.Bind(1, user_id);
        db.Bind(2, psearch.c_str());
        db.Bind(3, psearch.c_str());

        // If sort by parameter present
        if (ptags != 0) {
            db.Bind(4, ptags);
            db.Bind(5, offset);
        } else {
            db.Bind(4, offset);
        }

        while ( db.ExecutesStatement() == SQLITE_ROW ) {
            Link link(db.GetRowInt(1), db.GetRowStr(5));
            Bookmark b(db.GetRowInt(0), db.GetRowStr(2), link);
            b.setRating(db.GetRowInt(4));
            bookmarks.emplace_back(b);
        }

        db.FinalizeStatement();
    }

    db.CloseConnection();

    return bookmarks;
}

int BookmarkRepository::FindOrCreateLink(Link&& link, std::string& outputMessages)
{
    const char* sql = "Select ID from Links where Uri=?; ";
    DatabaseUtil& db = DatabaseUtil::GetInstance();

    if ( db.PreparedStatement(sql) ) {
        db.Bind(1, link.getUrls().c_str());

        if ( db.ExecutesStatement() == SQLITE_ROW ) {
            unsigned int ids = db.GetRowInt(0);
            db.FinalizeStatement();
            return ids;
        }

        std::cout << "[Link not found]" << std::endl;
        db.FinalizeStatement();

    }

    const char* sql_create_link = "INSERT INTO Links(Uri) values(?); ";

    if ( db.PreparedStatement(sql_create_link) ) {
        std::cout << "[Attempt create links]" << link.getUrls().c_str() << std::endl;
        db.Bind(1, link.getUrls().c_str());

        int status = db.ExecutesStatement();

        if ( status == SQLITE_DONE ) {
            int link_id = db.GetLastRowInsertId();
            std::cout << "[New link has been created] ID: " << link_id << std::endl;
            db.FinalizeStatement();
            return link_id;
        }

        std::cerr << "[Failed create new link] errcode: " << status << std::endl;
    }

    return 0;
}

std::vector<scaterbrain::Tag> BookmarkRepository::GetTags(const unsigned int& bookmarkId)
{
    const char* sql =
        "SELECT Bookmarks_ID, Tags_ID as T_ID, Tags.Name from Tags_Bookmarks "
        "LEFT JOIN Tags on Tags_Bookmarks.Tags_ID=Tags.ID "
        "WHERE Bookmarks_ID=?;";
    std::vector<scaterbrain::Tag> tags;
    tags.reserve(10);
    DatabaseUtil& db = DatabaseUtil::GetInstance();

    if (db.PreparedStatement(sql)) {
        db.Bind(1, bookmarkId);
        while ( db.ExecutesStatement() == SQLITE_ROW ) {
            scaterbrain::Tag tag(db.GetRowInt(1), db.GetRowStr(2));
            tags.push_back(tag);
        }

        db.FinalizeStatement();
    }

    return tags;
}

int BookmarkRepository::InsertBookmark(const Bookmark& b, const unsigned int& link_id, std::string& outputMessages)
{
    DatabaseUtil& db = DatabaseUtil::GetInstance();

    const char* sql =
        "INSERT INTO Bookmarks(Links_ID, Users_ID, Title, Hidden, Rating, Description) "
        "values(?,?,?,?,?,?); ";

    const unsigned int& user_id = Session::GetCurrentUser();

    if ( db.PreparedStatement(sql) ) {
        db.Bind(1, link_id);
        db.Bind(2, user_id);
        db.Bind(3, b.getTitle().c_str());
        db.Bind(4, b.getHidden());
        db.Bind(5, b.getRating());
        db.Bind(6, b.getDescription().c_str());

        int status = db.ExecutesStatement();

        if (status == SQLITE_DONE) {
            unsigned int tempid;
            tempid = db.GetLastRowInsertId();

            db.FinalizeStatement();
            std::cout << "[Attempt create bookmarks]" <<  std::endl;
            std::cout << "[New bookmarks has been created] ID: " << tempid << std::endl;

            return tempid;
        }

        if (status == SQLITE_CONSTRAINT) {
            outputMessages.append("URL already exist");
        }
    }

    return 0;
}

void BookmarkRepository::InsertBookmarkTags(const unsigned int& ptagId, const unsigned int& pbookmarkId, std::string& outputMessages)
{
    const char* sql = "INSERT INTO Tags_Bookmarks(Tags_ID, Bookmarks_ID) values(?, ?); ";
    DatabaseUtil& db = DatabaseUtil::GetInstance();

    if (db.PreparedStatement(sql)) {
        db.Bind(1, ptagId);
        db.Bind(2, pbookmarkId);

        if (db.ExecutesStatement() == SQLITE_DONE) {
            std::cout << "[Tag Bookmarks has been added]" << std::endl;
            db.FinalizeStatement();
            return;
        }

        std::cerr << "[Execute statement create bookmark tags failed]" << std::endl;
        outputMessages.append("Adding tag failed ");
    }

    std::cerr << "[Statement create bookmark tags failed]" << std::endl;
}

bool BookmarkRepository::Login( const std::string& username, const std::string& password, unsigned int& out_users_id, std::string& outputMessages)
{
    const char* sql = "Select ID, Email, Password from Users where Email=?;";

    DatabaseUtil& db = DatabaseUtil::GetInstance();

    db.OpenConnection();

    if (db.PreparedStatement(sql)) {
        db.Bind(1, &username[0]);

        if ( db.ExecutesStatement() == SQLITE_ROW ) {
            std::cout << "Found Row: "  << std::endl;
            out_users_id = db.GetRowInt(0);
            const size_t h_psw = std::hash<std::string>{}(password);
            const long unsigned int psw = db.GetRowLong(2);

            db.FinalizeStatement();
            db.CloseConnection();

            // Sementara hardcode buat auth
            if (h_psw == psw) {
                return true;
            }

            outputMessages = "Password is not valid";

            return false;
        }

    }

    db.CloseConnection();
    outputMessages = "Credentials is not valid";

    return false;
}

void BookmarkRepository::Register(const std::string& username, const std::string& psw, std::string& out_message)
{
    const char* sql = "INSERT INTO USERS(Email, Password, Style) VALUES(?,?,?);";
    DatabaseUtil& db = DatabaseUtil::GetInstance();

    db.OpenConnection();
    db.BeginTrasaction();

    if (db.PreparedStatement(sql)) {
        db.Bind(1, username.c_str());
        db.Bind(2, psw.c_str());
        db.Bind(3, "Dark");

        if (db.ExecutesStatement() == SQLITE_DONE) {
            db.FinalizeStatement();
            db.CommitTransaction();
            db.CloseConnection();
            return;
        }
    }

    out_message = db.GetErrors();

    db.CloseConnection();
}

void BookmarkRepository::Update(const Bookmark& b, const std::vector<scaterbrain::Tag>& delete_tags, std::string& outputMessages)
{
    DatabaseUtil& db = DatabaseUtil::GetInstance();

    db.OpenConnection();

    db.BeginTrasaction();

    const unsigned int& linkId = FindOrCreateLink(b.getLink(), outputMessages);
    const Link link(linkId);
    Bookmark update_bookmark = b;
    update_bookmark.setLink(link);

    UpdateBookmark(update_bookmark, outputMessages);

    if (!b.getTags().empty()) {
        for (const scaterbrain::Tag& t : b.getTags()) {
            InsertBookmarkTags(t.GetId(), b.getId(), outputMessages);
        }
    }

    if (!delete_tags.empty()) {
        for (const scaterbrain::Tag& t : delete_tags) {
            DeleteBookmarkTags(update_bookmark.getId(), t.GetId(), outputMessages);
        }
    }

    db.CommitTransaction();

    db.CloseConnection();
}

void BookmarkRepository::Update(const scaterbrain::Tag& tag, std::string& outputMessages)
{
    DatabaseUtil& db = DatabaseUtil::GetInstance();
    const char* sql = "Update Tags SET Name=?, Rating=? where ID=?; ";

    db.OpenConnection();
    db.BeginTrasaction();

    if ( db.PreparedStatement(sql) ) {
        db.Bind(1, tag.GetName().c_str());
        db.Bind(2, tag.GetRating());
        db.Bind(3, tag.GetId());

        int status = db.ExecutesStatement();

        if (status == SQLITE_DONE) {
            db.FinalizeStatement();
            db.CommitTransaction();
            db.CloseConnection();
            return;
        }
    }

    db.CloseConnection();

    outputMessages = "Updating tag failed";
}

void BookmarkRepository::UpdateBookmark(const Bookmark& b, std::string& outputMessages)
{
    DatabaseUtil& db = DatabaseUtil::GetInstance();
    const char* sql = "Update Bookmarks SET Links_ID=?, Title=?, Rating=?, Hidden=?, Description=? where ID=?; ";

    if ( db.PreparedStatement(sql) ) {
        db.Bind(1, b.getLink().getIds());
        db.Bind(2, b.getTitle().c_str());
        db.Bind(3, b.getRating());
        db.Bind(4, b.getHidden());
        db.Bind(5, b.getDescription().c_str());
        db.Bind(6, b.getId());

        int status = db.ExecutesStatement();

        if (status == SQLITE_DONE) {
            db.FinalizeStatement();
            std::cout << "[Bookmark has been update]" << std::endl;
            return;
        }

        outputMessages = "Updating bookmark failed";
    }
}


//static void LogErrors(const int& status, std::string& message) {
//    switch (status) {
//        case SQLITE_ERROR:
//            std::cerr << "[Bookmark failed to create, error]" << std::endl;
//            break;
//        case SQLITE_CONSTRAINT:
//            std::cerr << "[Bookmark failed to create, primary key duplicate] errorcode: " << status << std::endl;
//            break;
//        case SQLITE_BUSY:
//            std::cerr << "[Bookmark failed to create, busy]" << std::endl;
//            break;
//        default:
//            std::cerr << "[Bookmark failed to create]" << std::endl;
//    }
//}
